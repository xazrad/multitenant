from rest_framework import routers

from fvstuff import views


app_name = 'fvstuff'

router = routers.DefaultRouter()
router.register(r'v1/items', views.ItemViewSet, base_name='item')

urlpatterns = router.urls
