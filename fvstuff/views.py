from rest_framework import mixins
from rest_framework import status
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.serializers import ModelSerializer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from fvstuff.models import Items


class ItemSerializer(ModelSerializer):

    class Meta:
        model = Items
        fields = '__all__'


class ItemViewSet(mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated, )
    queryset = Items.objects.all()
    serializer_class = ItemSerializer
