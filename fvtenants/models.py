from django.db import models
from django_tenants.models import TenantMixin


class Tenant(TenantMixin):
    name = models.CharField(max_length=100)


class UserCredential(models.Model):
    email = models.EmailField('email address')
    password = models.CharField('password', max_length=128)
    tenants = models.ManyToManyField(Tenant, related_name='users')
