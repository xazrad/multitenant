from django_tenants.utils import schema_context

from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.hashers import check_password

from fvtenants.models import UserCredential


class CustomBackend(ModelBackend):

    def authenticate(self, request, username, password=None, **kwargs):
        # check global setting
        username = username.lower()
        # check user creditials in public
        # user exists
        try:
            user_creds = UserCredential.objects.get(email=username)
        except UserCredential.DoesNotExist:
            return
        # check password
        if not check_password(password, user_creds.password):
            return
        return user_creds
