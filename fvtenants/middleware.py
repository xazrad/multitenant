import jwt
from rest_framework.authentication import get_authorization_header
from rest_framework_jwt.settings import api_settings

from django.contrib.contenttypes.models import ContentType
from django.db import connection
from django.utils.deprecation import MiddlewareMixin
from django.utils.encoding import smart_text

from fvtenants.models import Tenant


class TenantMainMiddleware(MiddlewareMixin):

    def get_jwt_value(self, request):
        auth = get_authorization_header(request).split()
        auth_header_prefix = api_settings.JWT_AUTH_HEADER_PREFIX.lower()
        if not auth:
            return None
        if smart_text(auth[0].lower()) != auth_header_prefix:
            return None

        if len(auth) == 1:
            return
        elif len(auth) > 2:
            return
        return auth[1]

    def process_request(self, request):
        # Connection needs first to be at the public schema, as this is where
        # the tenant metadata is stored.
        connection.set_schema_to_public()
        jwt_value = self.get_jwt_value(request)
        if jwt_value:
            unverified_payload = jwt.decode(jwt_value, None, False)
            tenant_pk = unverified_payload['current_tenant']
            if tenant_pk:
                tenant = Tenant.objects.get(pk=tenant_pk)
                request.tenant = tenant
                connection.set_tenant(request.tenant)
        # Content type can no longer be cached as public and tenant schemas
        # have different models. If someone wants to change this, the cache
        # needs to be separated between public and shared schemas. If this
        # cache isn't cleared, this can cause permission problems. For example,
        # on public, a particular model has id 14, but on the tenants it has
        # the id 15. if 14 is cached instead of 15, the permissions for the
        # wrong model will be fetched.
        ContentType.objects.clear_cache()
