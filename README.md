### DataBase

connection:
  'USER': 'postgres',
  'PASSWORD': 'superPassword',
  'HOST': 'localhost',
  'PORT': 9452,

schemas:
  * tenant1
  
  * tenant2

users:
  * user@tenant1.com / q123q123Az
  
  * user@tenant2.com / q123q123Az

dump: `etc/dump.sql`

### Curl commands

#### Login

```
curl --data '{"username": "user@tenant2.com", "password": "q123q123Az"}' -H "Content-Type: application/json" http://127.0.0.1:8000/v1/token/jwt/
```

### Get Items

```
curl -H "Authorization: Bearer ${TOKEN}" http://127.0.0.1:8000/v1/items/
```
