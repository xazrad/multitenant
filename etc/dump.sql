toc.dat                                                                                             0000600 0004000 0002000 00000320704 13424620645 0014453 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        PGDMP                            w         
   app_tenant    10.4 (Debian 10.4-2.pgdg90+1) #   10.6 (Ubuntu 10.6-0ubuntu0.18.04.1) ;   �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false         �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false         �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false         �           1262    2607774 
   app_tenant    DATABASE     z   CREATE DATABASE app_tenant WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';
    DROP DATABASE app_tenant;
             postgres    false                     2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false         �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3         	            2615    2607797    tenant1    SCHEMA        CREATE SCHEMA tenant1;
    DROP SCHEMA tenant1;
             postgres    false                     2615    2607966    tenant2    SCHEMA        CREATE SCHEMA tenant2;
    DROP SCHEMA tenant2;
             postgres    false                     3079    12980    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false         �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1         �            1259    2607777    django_migrations    TABLE     �   CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);
 %   DROP TABLE public.django_migrations;
       public         postgres    false    3         �            1259    2607775    django_migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.django_migrations_id_seq;
       public       postgres    false    199    3         �           0    0    django_migrations_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;
            public       postgres    false    198         �            1259    2607788    fvtenants_tenant    TABLE     �   CREATE TABLE public.fvtenants_tenant (
    id integer NOT NULL,
    schema_name character varying(63) NOT NULL,
    name character varying(100) NOT NULL
);
 $   DROP TABLE public.fvtenants_tenant;
       public         postgres    false    3         �            1259    2607786    fvtenants_tenant_id_seq    SEQUENCE     �   CREATE SEQUENCE public.fvtenants_tenant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.fvtenants_tenant_id_seq;
       public       postgres    false    3    201         �           0    0    fvtenants_tenant_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.fvtenants_tenant_id_seq OWNED BY public.fvtenants_tenant.id;
            public       postgres    false    200         �            1259    2608135    fvtenants_usercredential    TABLE     �   CREATE TABLE public.fvtenants_usercredential (
    id integer NOT NULL,
    email character varying(254) NOT NULL,
    password character varying(128) NOT NULL
);
 ,   DROP TABLE public.fvtenants_usercredential;
       public         postgres    false    3         �            1259    2608133    fvtenants_usercredential_id_seq    SEQUENCE     �   CREATE SEQUENCE public.fvtenants_usercredential_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.fvtenants_usercredential_id_seq;
       public       postgres    false    241    3         �           0    0    fvtenants_usercredential_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.fvtenants_usercredential_id_seq OWNED BY public.fvtenants_usercredential.id;
            public       postgres    false    240         �            1259    2608143     fvtenants_usercredential_tenants    TABLE     �   CREATE TABLE public.fvtenants_usercredential_tenants (
    id integer NOT NULL,
    usercredential_id integer NOT NULL,
    tenant_id integer NOT NULL
);
 4   DROP TABLE public.fvtenants_usercredential_tenants;
       public         postgres    false    3         �            1259    2608141 '   fvtenants_usercredential_tenants_id_seq    SEQUENCE     �   CREATE SEQUENCE public.fvtenants_usercredential_tenants_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 >   DROP SEQUENCE public.fvtenants_usercredential_tenants_id_seq;
       public       postgres    false    3    243         �           0    0 '   fvtenants_usercredential_tenants_id_seq    SEQUENCE OWNED BY     s   ALTER SEQUENCE public.fvtenants_usercredential_tenants_id_seq OWNED BY public.fvtenants_usercredential_tenants.id;
            public       postgres    false    242         �            1259    2607829 
   auth_group    TABLE     f   CREATE TABLE tenant1.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);
    DROP TABLE tenant1.auth_group;
       tenant1         postgres    false    9         �            1259    2607827    auth_group_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant1.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE tenant1.auth_group_id_seq;
       tenant1       postgres    false    9    209         �           0    0    auth_group_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE tenant1.auth_group_id_seq OWNED BY tenant1.auth_group.id;
            tenant1       postgres    false    208         �            1259    2607839    auth_group_permissions    TABLE     �   CREATE TABLE tenant1.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);
 +   DROP TABLE tenant1.auth_group_permissions;
       tenant1         postgres    false    9         �            1259    2607837    auth_group_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant1.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE tenant1.auth_group_permissions_id_seq;
       tenant1       postgres    false    9    211         �           0    0    auth_group_permissions_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE tenant1.auth_group_permissions_id_seq OWNED BY tenant1.auth_group_permissions.id;
            tenant1       postgres    false    210         �            1259    2607821    auth_permission    TABLE     �   CREATE TABLE tenant1.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);
 $   DROP TABLE tenant1.auth_permission;
       tenant1         postgres    false    9         �            1259    2607819    auth_permission_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant1.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE tenant1.auth_permission_id_seq;
       tenant1       postgres    false    207    9         �           0    0    auth_permission_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE tenant1.auth_permission_id_seq OWNED BY tenant1.auth_permission.id;
            tenant1       postgres    false    206         �            1259    2607847 	   auth_user    TABLE     �  CREATE TABLE tenant1.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);
    DROP TABLE tenant1.auth_user;
       tenant1         postgres    false    9         �            1259    2607857    auth_user_groups    TABLE     �   CREATE TABLE tenant1.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);
 %   DROP TABLE tenant1.auth_user_groups;
       tenant1         postgres    false    9         �            1259    2607855    auth_user_groups_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant1.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE tenant1.auth_user_groups_id_seq;
       tenant1       postgres    false    9    215         �           0    0    auth_user_groups_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE tenant1.auth_user_groups_id_seq OWNED BY tenant1.auth_user_groups.id;
            tenant1       postgres    false    214         �            1259    2607845    auth_user_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant1.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE tenant1.auth_user_id_seq;
       tenant1       postgres    false    9    213                     0    0    auth_user_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE tenant1.auth_user_id_seq OWNED BY tenant1.auth_user.id;
            tenant1       postgres    false    212         �            1259    2607865    auth_user_user_permissions    TABLE     �   CREATE TABLE tenant1.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);
 /   DROP TABLE tenant1.auth_user_user_permissions;
       tenant1         postgres    false    9         �            1259    2607863 !   auth_user_user_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant1.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE tenant1.auth_user_user_permissions_id_seq;
       tenant1       postgres    false    9    217                    0    0 !   auth_user_user_permissions_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE tenant1.auth_user_user_permissions_id_seq OWNED BY tenant1.auth_user_user_permissions.id;
            tenant1       postgres    false    216         �            1259    2607925    django_admin_log    TABLE     �  CREATE TABLE tenant1.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);
 %   DROP TABLE tenant1.django_admin_log;
       tenant1         postgres    false    9         �            1259    2607923    django_admin_log_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant1.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE tenant1.django_admin_log_id_seq;
       tenant1       postgres    false    9    219                    0    0    django_admin_log_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE tenant1.django_admin_log_id_seq OWNED BY tenant1.django_admin_log.id;
            tenant1       postgres    false    218         �            1259    2607811    django_content_type    TABLE     �   CREATE TABLE tenant1.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);
 (   DROP TABLE tenant1.django_content_type;
       tenant1         postgres    false    9         �            1259    2607809    django_content_type_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant1.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE tenant1.django_content_type_id_seq;
       tenant1       postgres    false    9    205                    0    0    django_content_type_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE tenant1.django_content_type_id_seq OWNED BY tenant1.django_content_type.id;
            tenant1       postgres    false    204         �            1259    2607800    django_migrations    TABLE     �   CREATE TABLE tenant1.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);
 &   DROP TABLE tenant1.django_migrations;
       tenant1         postgres    false    9         �            1259    2607798    django_migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant1.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE tenant1.django_migrations_id_seq;
       tenant1       postgres    false    203    9                    0    0    django_migrations_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE tenant1.django_migrations_id_seq OWNED BY tenant1.django_migrations.id;
            tenant1       postgres    false    202         �            1259    2607953    django_session    TABLE     �   CREATE TABLE tenant1.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);
 #   DROP TABLE tenant1.django_session;
       tenant1         postgres    false    9         �            1259    2608369    fvstuff_items    TABLE     X   CREATE TABLE tenant1.fvstuff_items (
    id integer NOT NULL,
    name text NOT NULL
);
 "   DROP TABLE tenant1.fvstuff_items;
       tenant1         postgres    false    9         �            1259    2608367    fvstuff_items_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant1.fvstuff_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE tenant1.fvstuff_items_id_seq;
       tenant1       postgres    false    9    251                    0    0    fvstuff_items_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE tenant1.fvstuff_items_id_seq OWNED BY tenant1.fvstuff_items.id;
            tenant1       postgres    false    250         �            1259    2608340    fvstuff_user    TABLE     �  CREATE TABLE tenant1.fvstuff_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    email character varying(254) NOT NULL
);
 !   DROP TABLE tenant1.fvstuff_user;
       tenant1         postgres    false    9         �            1259    2608353    fvstuff_user_groups    TABLE     �   CREATE TABLE tenant1.fvstuff_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);
 (   DROP TABLE tenant1.fvstuff_user_groups;
       tenant1         postgres    false    9         �            1259    2608351    fvstuff_user_groups_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant1.fvstuff_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE tenant1.fvstuff_user_groups_id_seq;
       tenant1       postgres    false    9    247                    0    0    fvstuff_user_groups_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE tenant1.fvstuff_user_groups_id_seq OWNED BY tenant1.fvstuff_user_groups.id;
            tenant1       postgres    false    246         �            1259    2608338    fvstuff_user_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant1.fvstuff_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE tenant1.fvstuff_user_id_seq;
       tenant1       postgres    false    9    245                    0    0    fvstuff_user_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE tenant1.fvstuff_user_id_seq OWNED BY tenant1.fvstuff_user.id;
            tenant1       postgres    false    244         �            1259    2608361    fvstuff_user_user_permissions    TABLE     �   CREATE TABLE tenant1.fvstuff_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);
 2   DROP TABLE tenant1.fvstuff_user_user_permissions;
       tenant1         postgres    false    9         �            1259    2608359 $   fvstuff_user_user_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant1.fvstuff_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE tenant1.fvstuff_user_user_permissions_id_seq;
       tenant1       postgres    false    9    249                    0    0 $   fvstuff_user_user_permissions_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE tenant1.fvstuff_user_user_permissions_id_seq OWNED BY tenant1.fvstuff_user_user_permissions.id;
            tenant1       postgres    false    248         �            1259    2607998 
   auth_group    TABLE     f   CREATE TABLE tenant2.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);
    DROP TABLE tenant2.auth_group;
       tenant2         postgres    false    4         �            1259    2607996    auth_group_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant2.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE tenant2.auth_group_id_seq;
       tenant2       postgres    false    4    228         	           0    0    auth_group_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE tenant2.auth_group_id_seq OWNED BY tenant2.auth_group.id;
            tenant2       postgres    false    227         �            1259    2608008    auth_group_permissions    TABLE     �   CREATE TABLE tenant2.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);
 +   DROP TABLE tenant2.auth_group_permissions;
       tenant2         postgres    false    4         �            1259    2608006    auth_group_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant2.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE tenant2.auth_group_permissions_id_seq;
       tenant2       postgres    false    230    4         
           0    0    auth_group_permissions_id_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE tenant2.auth_group_permissions_id_seq OWNED BY tenant2.auth_group_permissions.id;
            tenant2       postgres    false    229         �            1259    2607990    auth_permission    TABLE     �   CREATE TABLE tenant2.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);
 $   DROP TABLE tenant2.auth_permission;
       tenant2         postgres    false    4         �            1259    2607988    auth_permission_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant2.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE tenant2.auth_permission_id_seq;
       tenant2       postgres    false    4    226                    0    0    auth_permission_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE tenant2.auth_permission_id_seq OWNED BY tenant2.auth_permission.id;
            tenant2       postgres    false    225         �            1259    2608016 	   auth_user    TABLE     �  CREATE TABLE tenant2.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);
    DROP TABLE tenant2.auth_user;
       tenant2         postgres    false    4         �            1259    2608026    auth_user_groups    TABLE     �   CREATE TABLE tenant2.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);
 %   DROP TABLE tenant2.auth_user_groups;
       tenant2         postgres    false    4         �            1259    2608024    auth_user_groups_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant2.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE tenant2.auth_user_groups_id_seq;
       tenant2       postgres    false    234    4                    0    0    auth_user_groups_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE tenant2.auth_user_groups_id_seq OWNED BY tenant2.auth_user_groups.id;
            tenant2       postgres    false    233         �            1259    2608014    auth_user_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant2.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE tenant2.auth_user_id_seq;
       tenant2       postgres    false    4    232                    0    0    auth_user_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE tenant2.auth_user_id_seq OWNED BY tenant2.auth_user.id;
            tenant2       postgres    false    231         �            1259    2608034    auth_user_user_permissions    TABLE     �   CREATE TABLE tenant2.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);
 /   DROP TABLE tenant2.auth_user_user_permissions;
       tenant2         postgres    false    4         �            1259    2608032 !   auth_user_user_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant2.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE tenant2.auth_user_user_permissions_id_seq;
       tenant2       postgres    false    4    236                    0    0 !   auth_user_user_permissions_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE tenant2.auth_user_user_permissions_id_seq OWNED BY tenant2.auth_user_user_permissions.id;
            tenant2       postgres    false    235         �            1259    2608094    django_admin_log    TABLE     �  CREATE TABLE tenant2.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);
 %   DROP TABLE tenant2.django_admin_log;
       tenant2         postgres    false    4         �            1259    2608092    django_admin_log_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant2.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE tenant2.django_admin_log_id_seq;
       tenant2       postgres    false    4    238                    0    0    django_admin_log_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE tenant2.django_admin_log_id_seq OWNED BY tenant2.django_admin_log.id;
            tenant2       postgres    false    237         �            1259    2607980    django_content_type    TABLE     �   CREATE TABLE tenant2.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);
 (   DROP TABLE tenant2.django_content_type;
       tenant2         postgres    false    4         �            1259    2607978    django_content_type_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant2.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE tenant2.django_content_type_id_seq;
       tenant2       postgres    false    224    4                    0    0    django_content_type_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE tenant2.django_content_type_id_seq OWNED BY tenant2.django_content_type.id;
            tenant2       postgres    false    223         �            1259    2607969    django_migrations    TABLE     �   CREATE TABLE tenant2.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);
 &   DROP TABLE tenant2.django_migrations;
       tenant2         postgres    false    4         �            1259    2607967    django_migrations_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant2.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE tenant2.django_migrations_id_seq;
       tenant2       postgres    false    4    222                    0    0    django_migrations_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE tenant2.django_migrations_id_seq OWNED BY tenant2.django_migrations.id;
            tenant2       postgres    false    221         �            1259    2608122    django_session    TABLE     �   CREATE TABLE tenant2.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);
 #   DROP TABLE tenant2.django_session;
       tenant2         postgres    false    4                    1259    2608438    fvstuff_items    TABLE     X   CREATE TABLE tenant2.fvstuff_items (
    id integer NOT NULL,
    name text NOT NULL
);
 "   DROP TABLE tenant2.fvstuff_items;
       tenant2         postgres    false    4                    1259    2608436    fvstuff_items_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant2.fvstuff_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE tenant2.fvstuff_items_id_seq;
       tenant2       postgres    false    4    259                    0    0    fvstuff_items_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE tenant2.fvstuff_items_id_seq OWNED BY tenant2.fvstuff_items.id;
            tenant2       postgres    false    258         �            1259    2608409    fvstuff_user    TABLE     �  CREATE TABLE tenant2.fvstuff_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    email character varying(254) NOT NULL
);
 !   DROP TABLE tenant2.fvstuff_user;
       tenant2         postgres    false    4         �            1259    2608422    fvstuff_user_groups    TABLE     �   CREATE TABLE tenant2.fvstuff_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);
 (   DROP TABLE tenant2.fvstuff_user_groups;
       tenant2         postgres    false    4         �            1259    2608420    fvstuff_user_groups_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant2.fvstuff_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE tenant2.fvstuff_user_groups_id_seq;
       tenant2       postgres    false    255    4                    0    0    fvstuff_user_groups_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE tenant2.fvstuff_user_groups_id_seq OWNED BY tenant2.fvstuff_user_groups.id;
            tenant2       postgres    false    254         �            1259    2608407    fvstuff_user_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant2.fvstuff_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE tenant2.fvstuff_user_id_seq;
       tenant2       postgres    false    4    253                    0    0    fvstuff_user_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE tenant2.fvstuff_user_id_seq OWNED BY tenant2.fvstuff_user.id;
            tenant2       postgres    false    252                    1259    2608430    fvstuff_user_user_permissions    TABLE     �   CREATE TABLE tenant2.fvstuff_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);
 2   DROP TABLE tenant2.fvstuff_user_user_permissions;
       tenant2         postgres    false    4                     1259    2608428 $   fvstuff_user_user_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE tenant2.fvstuff_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE tenant2.fvstuff_user_user_permissions_id_seq;
       tenant2       postgres    false    4    257                    0    0 $   fvstuff_user_user_permissions_id_seq    SEQUENCE OWNED BY     o   ALTER SEQUENCE tenant2.fvstuff_user_user_permissions_id_seq OWNED BY tenant2.fvstuff_user_user_permissions.id;
            tenant2       postgres    false    256         j           2604    2607780    django_migrations id    DEFAULT     |   ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);
 C   ALTER TABLE public.django_migrations ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    199    199         k           2604    2607791    fvtenants_tenant id    DEFAULT     z   ALTER TABLE ONLY public.fvtenants_tenant ALTER COLUMN id SET DEFAULT nextval('public.fvtenants_tenant_id_seq'::regclass);
 B   ALTER TABLE public.fvtenants_tenant ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    200    201    201         �           2604    2608138    fvtenants_usercredential id    DEFAULT     �   ALTER TABLE ONLY public.fvtenants_usercredential ALTER COLUMN id SET DEFAULT nextval('public.fvtenants_usercredential_id_seq'::regclass);
 J   ALTER TABLE public.fvtenants_usercredential ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    241    240    241         �           2604    2608146 #   fvtenants_usercredential_tenants id    DEFAULT     �   ALTER TABLE ONLY public.fvtenants_usercredential_tenants ALTER COLUMN id SET DEFAULT nextval('public.fvtenants_usercredential_tenants_id_seq'::regclass);
 R   ALTER TABLE public.fvtenants_usercredential_tenants ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    243    242    243         o           2604    2607832    auth_group id    DEFAULT     p   ALTER TABLE ONLY tenant1.auth_group ALTER COLUMN id SET DEFAULT nextval('tenant1.auth_group_id_seq'::regclass);
 =   ALTER TABLE tenant1.auth_group ALTER COLUMN id DROP DEFAULT;
       tenant1       postgres    false    209    208    209         p           2604    2607842    auth_group_permissions id    DEFAULT     �   ALTER TABLE ONLY tenant1.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('tenant1.auth_group_permissions_id_seq'::regclass);
 I   ALTER TABLE tenant1.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
       tenant1       postgres    false    210    211    211         n           2604    2607824    auth_permission id    DEFAULT     z   ALTER TABLE ONLY tenant1.auth_permission ALTER COLUMN id SET DEFAULT nextval('tenant1.auth_permission_id_seq'::regclass);
 B   ALTER TABLE tenant1.auth_permission ALTER COLUMN id DROP DEFAULT;
       tenant1       postgres    false    206    207    207         q           2604    2607850    auth_user id    DEFAULT     n   ALTER TABLE ONLY tenant1.auth_user ALTER COLUMN id SET DEFAULT nextval('tenant1.auth_user_id_seq'::regclass);
 <   ALTER TABLE tenant1.auth_user ALTER COLUMN id DROP DEFAULT;
       tenant1       postgres    false    212    213    213         r           2604    2607860    auth_user_groups id    DEFAULT     |   ALTER TABLE ONLY tenant1.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('tenant1.auth_user_groups_id_seq'::regclass);
 C   ALTER TABLE tenant1.auth_user_groups ALTER COLUMN id DROP DEFAULT;
       tenant1       postgres    false    215    214    215         s           2604    2607868    auth_user_user_permissions id    DEFAULT     �   ALTER TABLE ONLY tenant1.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('tenant1.auth_user_user_permissions_id_seq'::regclass);
 M   ALTER TABLE tenant1.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
       tenant1       postgres    false    216    217    217         t           2604    2607928    django_admin_log id    DEFAULT     |   ALTER TABLE ONLY tenant1.django_admin_log ALTER COLUMN id SET DEFAULT nextval('tenant1.django_admin_log_id_seq'::regclass);
 C   ALTER TABLE tenant1.django_admin_log ALTER COLUMN id DROP DEFAULT;
       tenant1       postgres    false    218    219    219         m           2604    2607814    django_content_type id    DEFAULT     �   ALTER TABLE ONLY tenant1.django_content_type ALTER COLUMN id SET DEFAULT nextval('tenant1.django_content_type_id_seq'::regclass);
 F   ALTER TABLE tenant1.django_content_type ALTER COLUMN id DROP DEFAULT;
       tenant1       postgres    false    204    205    205         l           2604    2607803    django_migrations id    DEFAULT     ~   ALTER TABLE ONLY tenant1.django_migrations ALTER COLUMN id SET DEFAULT nextval('tenant1.django_migrations_id_seq'::regclass);
 D   ALTER TABLE tenant1.django_migrations ALTER COLUMN id DROP DEFAULT;
       tenant1       postgres    false    202    203    203         �           2604    2608372    fvstuff_items id    DEFAULT     v   ALTER TABLE ONLY tenant1.fvstuff_items ALTER COLUMN id SET DEFAULT nextval('tenant1.fvstuff_items_id_seq'::regclass);
 @   ALTER TABLE tenant1.fvstuff_items ALTER COLUMN id DROP DEFAULT;
       tenant1       postgres    false    251    250    251         �           2604    2608343    fvstuff_user id    DEFAULT     t   ALTER TABLE ONLY tenant1.fvstuff_user ALTER COLUMN id SET DEFAULT nextval('tenant1.fvstuff_user_id_seq'::regclass);
 ?   ALTER TABLE tenant1.fvstuff_user ALTER COLUMN id DROP DEFAULT;
       tenant1       postgres    false    245    244    245         �           2604    2608356    fvstuff_user_groups id    DEFAULT     �   ALTER TABLE ONLY tenant1.fvstuff_user_groups ALTER COLUMN id SET DEFAULT nextval('tenant1.fvstuff_user_groups_id_seq'::regclass);
 F   ALTER TABLE tenant1.fvstuff_user_groups ALTER COLUMN id DROP DEFAULT;
       tenant1       postgres    false    247    246    247         �           2604    2608364     fvstuff_user_user_permissions id    DEFAULT     �   ALTER TABLE ONLY tenant1.fvstuff_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('tenant1.fvstuff_user_user_permissions_id_seq'::regclass);
 P   ALTER TABLE tenant1.fvstuff_user_user_permissions ALTER COLUMN id DROP DEFAULT;
       tenant1       postgres    false    248    249    249         y           2604    2608001    auth_group id    DEFAULT     p   ALTER TABLE ONLY tenant2.auth_group ALTER COLUMN id SET DEFAULT nextval('tenant2.auth_group_id_seq'::regclass);
 =   ALTER TABLE tenant2.auth_group ALTER COLUMN id DROP DEFAULT;
       tenant2       postgres    false    228    227    228         z           2604    2608011    auth_group_permissions id    DEFAULT     �   ALTER TABLE ONLY tenant2.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('tenant2.auth_group_permissions_id_seq'::regclass);
 I   ALTER TABLE tenant2.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
       tenant2       postgres    false    229    230    230         x           2604    2607993    auth_permission id    DEFAULT     z   ALTER TABLE ONLY tenant2.auth_permission ALTER COLUMN id SET DEFAULT nextval('tenant2.auth_permission_id_seq'::regclass);
 B   ALTER TABLE tenant2.auth_permission ALTER COLUMN id DROP DEFAULT;
       tenant2       postgres    false    226    225    226         {           2604    2608019    auth_user id    DEFAULT     n   ALTER TABLE ONLY tenant2.auth_user ALTER COLUMN id SET DEFAULT nextval('tenant2.auth_user_id_seq'::regclass);
 <   ALTER TABLE tenant2.auth_user ALTER COLUMN id DROP DEFAULT;
       tenant2       postgres    false    231    232    232         |           2604    2608029    auth_user_groups id    DEFAULT     |   ALTER TABLE ONLY tenant2.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('tenant2.auth_user_groups_id_seq'::regclass);
 C   ALTER TABLE tenant2.auth_user_groups ALTER COLUMN id DROP DEFAULT;
       tenant2       postgres    false    234    233    234         }           2604    2608037    auth_user_user_permissions id    DEFAULT     �   ALTER TABLE ONLY tenant2.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('tenant2.auth_user_user_permissions_id_seq'::regclass);
 M   ALTER TABLE tenant2.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
       tenant2       postgres    false    236    235    236         ~           2604    2608097    django_admin_log id    DEFAULT     |   ALTER TABLE ONLY tenant2.django_admin_log ALTER COLUMN id SET DEFAULT nextval('tenant2.django_admin_log_id_seq'::regclass);
 C   ALTER TABLE tenant2.django_admin_log ALTER COLUMN id DROP DEFAULT;
       tenant2       postgres    false    237    238    238         w           2604    2607983    django_content_type id    DEFAULT     �   ALTER TABLE ONLY tenant2.django_content_type ALTER COLUMN id SET DEFAULT nextval('tenant2.django_content_type_id_seq'::regclass);
 F   ALTER TABLE tenant2.django_content_type ALTER COLUMN id DROP DEFAULT;
       tenant2       postgres    false    223    224    224         v           2604    2607972    django_migrations id    DEFAULT     ~   ALTER TABLE ONLY tenant2.django_migrations ALTER COLUMN id SET DEFAULT nextval('tenant2.django_migrations_id_seq'::regclass);
 D   ALTER TABLE tenant2.django_migrations ALTER COLUMN id DROP DEFAULT;
       tenant2       postgres    false    222    221    222         �           2604    2608441    fvstuff_items id    DEFAULT     v   ALTER TABLE ONLY tenant2.fvstuff_items ALTER COLUMN id SET DEFAULT nextval('tenant2.fvstuff_items_id_seq'::regclass);
 @   ALTER TABLE tenant2.fvstuff_items ALTER COLUMN id DROP DEFAULT;
       tenant2       postgres    false    259    258    259         �           2604    2608412    fvstuff_user id    DEFAULT     t   ALTER TABLE ONLY tenant2.fvstuff_user ALTER COLUMN id SET DEFAULT nextval('tenant2.fvstuff_user_id_seq'::regclass);
 ?   ALTER TABLE tenant2.fvstuff_user ALTER COLUMN id DROP DEFAULT;
       tenant2       postgres    false    252    253    253         �           2604    2608425    fvstuff_user_groups id    DEFAULT     �   ALTER TABLE ONLY tenant2.fvstuff_user_groups ALTER COLUMN id SET DEFAULT nextval('tenant2.fvstuff_user_groups_id_seq'::regclass);
 F   ALTER TABLE tenant2.fvstuff_user_groups ALTER COLUMN id DROP DEFAULT;
       tenant2       postgres    false    255    254    255         �           2604    2608433     fvstuff_user_user_permissions id    DEFAULT     �   ALTER TABLE ONLY tenant2.fvstuff_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('tenant2.fvstuff_user_user_permissions_id_seq'::regclass);
 P   ALTER TABLE tenant2.fvstuff_user_user_permissions ALTER COLUMN id DROP DEFAULT;
       tenant2       postgres    false    256    257    257         �          0    2607777    django_migrations 
   TABLE DATA               C   COPY public.django_migrations (id, app, name, applied) FROM stdin;
    public       postgres    false    199       3251.dat �          0    2607788    fvtenants_tenant 
   TABLE DATA               A   COPY public.fvtenants_tenant (id, schema_name, name) FROM stdin;
    public       postgres    false    201       3253.dat �          0    2608135    fvtenants_usercredential 
   TABLE DATA               G   COPY public.fvtenants_usercredential (id, email, password) FROM stdin;
    public       postgres    false    241       3293.dat �          0    2608143     fvtenants_usercredential_tenants 
   TABLE DATA               \   COPY public.fvtenants_usercredential_tenants (id, usercredential_id, tenant_id) FROM stdin;
    public       postgres    false    243       3295.dat �          0    2607829 
   auth_group 
   TABLE DATA               /   COPY tenant1.auth_group (id, name) FROM stdin;
    tenant1       postgres    false    209       3261.dat �          0    2607839    auth_group_permissions 
   TABLE DATA               N   COPY tenant1.auth_group_permissions (id, group_id, permission_id) FROM stdin;
    tenant1       postgres    false    211       3263.dat �          0    2607821    auth_permission 
   TABLE DATA               O   COPY tenant1.auth_permission (id, name, content_type_id, codename) FROM stdin;
    tenant1       postgres    false    207       3259.dat �          0    2607847 	   auth_user 
   TABLE DATA               �   COPY tenant1.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
    tenant1       postgres    false    213       3265.dat �          0    2607857    auth_user_groups 
   TABLE DATA               B   COPY tenant1.auth_user_groups (id, user_id, group_id) FROM stdin;
    tenant1       postgres    false    215       3267.dat �          0    2607865    auth_user_user_permissions 
   TABLE DATA               Q   COPY tenant1.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
    tenant1       postgres    false    217       3269.dat �          0    2607925    django_admin_log 
   TABLE DATA               �   COPY tenant1.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
    tenant1       postgres    false    219       3271.dat �          0    2607811    django_content_type 
   TABLE DATA               D   COPY tenant1.django_content_type (id, app_label, model) FROM stdin;
    tenant1       postgres    false    205       3257.dat �          0    2607800    django_migrations 
   TABLE DATA               D   COPY tenant1.django_migrations (id, app, name, applied) FROM stdin;
    tenant1       postgres    false    203       3255.dat �          0    2607953    django_session 
   TABLE DATA               Q   COPY tenant1.django_session (session_key, session_data, expire_date) FROM stdin;
    tenant1       postgres    false    220       3272.dat �          0    2608369    fvstuff_items 
   TABLE DATA               2   COPY tenant1.fvstuff_items (id, name) FROM stdin;
    tenant1       postgres    false    251       3303.dat �          0    2608340    fvstuff_user 
   TABLE DATA               �   COPY tenant1.fvstuff_user (id, password, last_login, is_superuser, username, first_name, last_name, is_staff, is_active, date_joined, email) FROM stdin;
    tenant1       postgres    false    245       3297.dat �          0    2608353    fvstuff_user_groups 
   TABLE DATA               E   COPY tenant1.fvstuff_user_groups (id, user_id, group_id) FROM stdin;
    tenant1       postgres    false    247       3299.dat �          0    2608361    fvstuff_user_user_permissions 
   TABLE DATA               T   COPY tenant1.fvstuff_user_user_permissions (id, user_id, permission_id) FROM stdin;
    tenant1       postgres    false    249       3301.dat �          0    2607998 
   auth_group 
   TABLE DATA               /   COPY tenant2.auth_group (id, name) FROM stdin;
    tenant2       postgres    false    228       3280.dat �          0    2608008    auth_group_permissions 
   TABLE DATA               N   COPY tenant2.auth_group_permissions (id, group_id, permission_id) FROM stdin;
    tenant2       postgres    false    230       3282.dat �          0    2607990    auth_permission 
   TABLE DATA               O   COPY tenant2.auth_permission (id, name, content_type_id, codename) FROM stdin;
    tenant2       postgres    false    226       3278.dat �          0    2608016 	   auth_user 
   TABLE DATA               �   COPY tenant2.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
    tenant2       postgres    false    232       3284.dat �          0    2608026    auth_user_groups 
   TABLE DATA               B   COPY tenant2.auth_user_groups (id, user_id, group_id) FROM stdin;
    tenant2       postgres    false    234       3286.dat �          0    2608034    auth_user_user_permissions 
   TABLE DATA               Q   COPY tenant2.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
    tenant2       postgres    false    236       3288.dat �          0    2608094    django_admin_log 
   TABLE DATA               �   COPY tenant2.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
    tenant2       postgres    false    238       3290.dat �          0    2607980    django_content_type 
   TABLE DATA               D   COPY tenant2.django_content_type (id, app_label, model) FROM stdin;
    tenant2       postgres    false    224       3276.dat �          0    2607969    django_migrations 
   TABLE DATA               D   COPY tenant2.django_migrations (id, app, name, applied) FROM stdin;
    tenant2       postgres    false    222       3274.dat �          0    2608122    django_session 
   TABLE DATA               Q   COPY tenant2.django_session (session_key, session_data, expire_date) FROM stdin;
    tenant2       postgres    false    239       3291.dat �          0    2608438    fvstuff_items 
   TABLE DATA               2   COPY tenant2.fvstuff_items (id, name) FROM stdin;
    tenant2       postgres    false    259       3311.dat �          0    2608409    fvstuff_user 
   TABLE DATA               �   COPY tenant2.fvstuff_user (id, password, last_login, is_superuser, username, first_name, last_name, is_staff, is_active, date_joined, email) FROM stdin;
    tenant2       postgres    false    253       3305.dat �          0    2608422    fvstuff_user_groups 
   TABLE DATA               E   COPY tenant2.fvstuff_user_groups (id, user_id, group_id) FROM stdin;
    tenant2       postgres    false    255       3307.dat �          0    2608430    fvstuff_user_user_permissions 
   TABLE DATA               T   COPY tenant2.fvstuff_user_user_permissions (id, user_id, permission_id) FROM stdin;
    tenant2       postgres    false    257       3309.dat            0    0    django_migrations_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.django_migrations_id_seq', 18, true);
            public       postgres    false    198                    0    0    fvtenants_tenant_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.fvtenants_tenant_id_seq', 2, true);
            public       postgres    false    200                    0    0    fvtenants_usercredential_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.fvtenants_usercredential_id_seq', 3, true);
            public       postgres    false    240                    0    0 '   fvtenants_usercredential_tenants_id_seq    SEQUENCE SET     U   SELECT pg_catalog.setval('public.fvtenants_usercredential_tenants_id_seq', 4, true);
            public       postgres    false    242                    0    0    auth_group_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('tenant1.auth_group_id_seq', 1, false);
            tenant1       postgres    false    208                    0    0    auth_group_permissions_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('tenant1.auth_group_permissions_id_seq', 1, false);
            tenant1       postgres    false    210                    0    0    auth_permission_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('tenant1.auth_permission_id_seq', 40, true);
            tenant1       postgres    false    206                    0    0    auth_user_groups_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('tenant1.auth_user_groups_id_seq', 1, false);
            tenant1       postgres    false    214                    0    0    auth_user_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('tenant1.auth_user_id_seq', 6, true);
            tenant1       postgres    false    212                    0    0 !   auth_user_user_permissions_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('tenant1.auth_user_user_permissions_id_seq', 1, false);
            tenant1       postgres    false    216                     0    0    django_admin_log_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('tenant1.django_admin_log_id_seq', 1, false);
            tenant1       postgres    false    218         !           0    0    django_content_type_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('tenant1.django_content_type_id_seq', 10, true);
            tenant1       postgres    false    204         "           0    0    django_migrations_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('tenant1.django_migrations_id_seq', 18, true);
            tenant1       postgres    false    202         #           0    0    fvstuff_items_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('tenant1.fvstuff_items_id_seq', 2, true);
            tenant1       postgres    false    250         $           0    0    fvstuff_user_groups_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('tenant1.fvstuff_user_groups_id_seq', 1, false);
            tenant1       postgres    false    246         %           0    0    fvstuff_user_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('tenant1.fvstuff_user_id_seq', 1, true);
            tenant1       postgres    false    244         &           0    0 $   fvstuff_user_user_permissions_id_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('tenant1.fvstuff_user_user_permissions_id_seq', 1, false);
            tenant1       postgres    false    248         '           0    0    auth_group_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('tenant2.auth_group_id_seq', 1, false);
            tenant2       postgres    false    227         (           0    0    auth_group_permissions_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('tenant2.auth_group_permissions_id_seq', 1, false);
            tenant2       postgres    false    229         )           0    0    auth_permission_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('tenant2.auth_permission_id_seq', 40, true);
            tenant2       postgres    false    225         *           0    0    auth_user_groups_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('tenant2.auth_user_groups_id_seq', 1, false);
            tenant2       postgres    false    233         +           0    0    auth_user_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('tenant2.auth_user_id_seq', 2, true);
            tenant2       postgres    false    231         ,           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE SET     Q   SELECT pg_catalog.setval('tenant2.auth_user_user_permissions_id_seq', 1, false);
            tenant2       postgres    false    235         -           0    0    django_admin_log_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('tenant2.django_admin_log_id_seq', 1, false);
            tenant2       postgres    false    237         .           0    0    django_content_type_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('tenant2.django_content_type_id_seq', 10, true);
            tenant2       postgres    false    223         /           0    0    django_migrations_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('tenant2.django_migrations_id_seq', 18, true);
            tenant2       postgres    false    221         0           0    0    fvstuff_items_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('tenant2.fvstuff_items_id_seq', 2, true);
            tenant2       postgres    false    258         1           0    0    fvstuff_user_groups_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('tenant2.fvstuff_user_groups_id_seq', 1, false);
            tenant2       postgres    false    254         2           0    0    fvstuff_user_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('tenant2.fvstuff_user_id_seq', 2, true);
            tenant2       postgres    false    252         3           0    0 $   fvstuff_user_user_permissions_id_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('tenant2.fvstuff_user_user_permissions_id_seq', 1, false);
            tenant2       postgres    false    256         �           2606    2607785 (   django_migrations django_migrations_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.django_migrations DROP CONSTRAINT django_migrations_pkey;
       public         postgres    false    199         �           2606    2607793 &   fvtenants_tenant fvtenants_tenant_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.fvtenants_tenant
    ADD CONSTRAINT fvtenants_tenant_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.fvtenants_tenant DROP CONSTRAINT fvtenants_tenant_pkey;
       public         postgres    false    201         �           2606    2607795 1   fvtenants_tenant fvtenants_tenant_schema_name_key 
   CONSTRAINT     s   ALTER TABLE ONLY public.fvtenants_tenant
    ADD CONSTRAINT fvtenants_tenant_schema_name_key UNIQUE (schema_name);
 [   ALTER TABLE ONLY public.fvtenants_tenant DROP CONSTRAINT fvtenants_tenant_schema_name_key;
       public         postgres    false    201         �           2606    2608140 6   fvtenants_usercredential fvtenants_usercredential_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.fvtenants_usercredential
    ADD CONSTRAINT fvtenants_usercredential_pkey PRIMARY KEY (id);
 `   ALTER TABLE ONLY public.fvtenants_usercredential DROP CONSTRAINT fvtenants_usercredential_pkey;
       public         postgres    false    241         �           2606    2608148 F   fvtenants_usercredential_tenants fvtenants_usercredential_tenants_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.fvtenants_usercredential_tenants
    ADD CONSTRAINT fvtenants_usercredential_tenants_pkey PRIMARY KEY (id);
 p   ALTER TABLE ONLY public.fvtenants_usercredential_tenants DROP CONSTRAINT fvtenants_usercredential_tenants_pkey;
       public         postgres    false    243         �           2606    2608160 `   fvtenants_usercredential_tenants fvtenants_usercredential_usercredential_id_tenant_daf12375_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY public.fvtenants_usercredential_tenants
    ADD CONSTRAINT fvtenants_usercredential_usercredential_id_tenant_daf12375_uniq UNIQUE (usercredential_id, tenant_id);
 �   ALTER TABLE ONLY public.fvtenants_usercredential_tenants DROP CONSTRAINT fvtenants_usercredential_usercredential_id_tenant_daf12375_uniq;
       public         postgres    false    243    243         �           2606    2607836    auth_group auth_group_name_key 
   CONSTRAINT     Z   ALTER TABLE ONLY tenant1.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);
 I   ALTER TABLE ONLY tenant1.auth_group DROP CONSTRAINT auth_group_name_key;
       tenant1         postgres    false    209         �           2606    2607891 R   auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY tenant1.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);
 }   ALTER TABLE ONLY tenant1.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
       tenant1         postgres    false    211    211         �           2606    2607844 2   auth_group_permissions auth_group_permissions_pkey 
   CONSTRAINT     q   ALTER TABLE ONLY tenant1.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);
 ]   ALTER TABLE ONLY tenant1.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
       tenant1         postgres    false    211         �           2606    2607834    auth_group auth_group_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY tenant1.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);
 E   ALTER TABLE ONLY tenant1.auth_group DROP CONSTRAINT auth_group_pkey;
       tenant1         postgres    false    209         �           2606    2607877 F   auth_permission auth_permission_content_type_id_codename_01ab375a_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY tenant1.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);
 q   ALTER TABLE ONLY tenant1.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
       tenant1         postgres    false    207    207         �           2606    2607826 $   auth_permission auth_permission_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY tenant1.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);
 O   ALTER TABLE ONLY tenant1.auth_permission DROP CONSTRAINT auth_permission_pkey;
       tenant1         postgres    false    207         �           2606    2607862 &   auth_user_groups auth_user_groups_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY tenant1.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);
 Q   ALTER TABLE ONLY tenant1.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
       tenant1         postgres    false    215         �           2606    2607906 @   auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY tenant1.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);
 k   ALTER TABLE ONLY tenant1.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
       tenant1         postgres    false    215    215         �           2606    2607852    auth_user auth_user_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY tenant1.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);
 C   ALTER TABLE ONLY tenant1.auth_user DROP CONSTRAINT auth_user_pkey;
       tenant1         postgres    false    213         �           2606    2607870 :   auth_user_user_permissions auth_user_user_permissions_pkey 
   CONSTRAINT     y   ALTER TABLE ONLY tenant1.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);
 e   ALTER TABLE ONLY tenant1.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
       tenant1         postgres    false    217         �           2606    2607920 Y   auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY tenant1.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);
 �   ALTER TABLE ONLY tenant1.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
       tenant1         postgres    false    217    217         �           2606    2607948     auth_user auth_user_username_key 
   CONSTRAINT     `   ALTER TABLE ONLY tenant1.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);
 K   ALTER TABLE ONLY tenant1.auth_user DROP CONSTRAINT auth_user_username_key;
       tenant1         postgres    false    213         �           2606    2607934 &   django_admin_log django_admin_log_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY tenant1.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);
 Q   ALTER TABLE ONLY tenant1.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
       tenant1         postgres    false    219         �           2606    2607818 E   django_content_type django_content_type_app_label_model_76bd3d3b_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY tenant1.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);
 p   ALTER TABLE ONLY tenant1.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
       tenant1         postgres    false    205    205         �           2606    2607816 ,   django_content_type django_content_type_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY tenant1.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);
 W   ALTER TABLE ONLY tenant1.django_content_type DROP CONSTRAINT django_content_type_pkey;
       tenant1         postgres    false    205         �           2606    2607808 (   django_migrations django_migrations_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY tenant1.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);
 S   ALTER TABLE ONLY tenant1.django_migrations DROP CONSTRAINT django_migrations_pkey;
       tenant1         postgres    false    203         �           2606    2607960 "   django_session django_session_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY tenant1.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);
 M   ALTER TABLE ONLY tenant1.django_session DROP CONSTRAINT django_session_pkey;
       tenant1         postgres    false    220         	           2606    2608377     fvstuff_items fvstuff_items_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY tenant1.fvstuff_items
    ADD CONSTRAINT fvstuff_items_pkey PRIMARY KEY (id);
 K   ALTER TABLE ONLY tenant1.fvstuff_items DROP CONSTRAINT fvstuff_items_pkey;
       tenant1         postgres    false    251         �           2606    2608358 ,   fvstuff_user_groups fvstuff_user_groups_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY tenant1.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_pkey PRIMARY KEY (id);
 W   ALTER TABLE ONLY tenant1.fvstuff_user_groups DROP CONSTRAINT fvstuff_user_groups_pkey;
       tenant1         postgres    false    247                    2606    2608390 F   fvstuff_user_groups fvstuff_user_groups_user_id_group_id_3ae9527c_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY tenant1.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_user_id_group_id_3ae9527c_uniq UNIQUE (user_id, group_id);
 q   ALTER TABLE ONLY tenant1.fvstuff_user_groups DROP CONSTRAINT fvstuff_user_groups_user_id_group_id_3ae9527c_uniq;
       tenant1         postgres    false    247    247         �           2606    2608348    fvstuff_user fvstuff_user_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY tenant1.fvstuff_user
    ADD CONSTRAINT fvstuff_user_pkey PRIMARY KEY (id);
 I   ALTER TABLE ONLY tenant1.fvstuff_user DROP CONSTRAINT fvstuff_user_pkey;
       tenant1         postgres    false    245                    2606    2608404 Z   fvstuff_user_user_permissions fvstuff_user_user_permis_user_id_permission_id_783f7839_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY tenant1.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_permis_user_id_permission_id_783f7839_uniq UNIQUE (user_id, permission_id);
 �   ALTER TABLE ONLY tenant1.fvstuff_user_user_permissions DROP CONSTRAINT fvstuff_user_user_permis_user_id_permission_id_783f7839_uniq;
       tenant1         postgres    false    249    249                    2606    2608366 @   fvstuff_user_user_permissions fvstuff_user_user_permissions_pkey 
   CONSTRAINT        ALTER TABLE ONLY tenant1.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_permissions_pkey PRIMARY KEY (id);
 k   ALTER TABLE ONLY tenant1.fvstuff_user_user_permissions DROP CONSTRAINT fvstuff_user_user_permissions_pkey;
       tenant1         postgres    false    249         �           2606    2608350 &   fvstuff_user fvstuff_user_username_key 
   CONSTRAINT     f   ALTER TABLE ONLY tenant1.fvstuff_user
    ADD CONSTRAINT fvstuff_user_username_key UNIQUE (username);
 Q   ALTER TABLE ONLY tenant1.fvstuff_user DROP CONSTRAINT fvstuff_user_username_key;
       tenant1         postgres    false    245         �           2606    2608005    auth_group auth_group_name_key 
   CONSTRAINT     Z   ALTER TABLE ONLY tenant2.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);
 I   ALTER TABLE ONLY tenant2.auth_group DROP CONSTRAINT auth_group_name_key;
       tenant2         postgres    false    228         �           2606    2608060 R   auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY tenant2.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);
 }   ALTER TABLE ONLY tenant2.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq;
       tenant2         postgres    false    230    230         �           2606    2608013 2   auth_group_permissions auth_group_permissions_pkey 
   CONSTRAINT     q   ALTER TABLE ONLY tenant2.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);
 ]   ALTER TABLE ONLY tenant2.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
       tenant2         postgres    false    230         �           2606    2608003    auth_group auth_group_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY tenant2.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);
 E   ALTER TABLE ONLY tenant2.auth_group DROP CONSTRAINT auth_group_pkey;
       tenant2         postgres    false    228         �           2606    2608046 F   auth_permission auth_permission_content_type_id_codename_01ab375a_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY tenant2.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);
 q   ALTER TABLE ONLY tenant2.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq;
       tenant2         postgres    false    226    226         �           2606    2607995 $   auth_permission auth_permission_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY tenant2.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);
 O   ALTER TABLE ONLY tenant2.auth_permission DROP CONSTRAINT auth_permission_pkey;
       tenant2         postgres    false    226         �           2606    2608031 &   auth_user_groups auth_user_groups_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY tenant2.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);
 Q   ALTER TABLE ONLY tenant2.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
       tenant2         postgres    false    234         �           2606    2608075 @   auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY tenant2.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);
 k   ALTER TABLE ONLY tenant2.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq;
       tenant2         postgres    false    234    234         �           2606    2608021    auth_user auth_user_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY tenant2.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);
 C   ALTER TABLE ONLY tenant2.auth_user DROP CONSTRAINT auth_user_pkey;
       tenant2         postgres    false    232         �           2606    2608039 :   auth_user_user_permissions auth_user_user_permissions_pkey 
   CONSTRAINT     y   ALTER TABLE ONLY tenant2.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);
 e   ALTER TABLE ONLY tenant2.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
       tenant2         postgres    false    236         �           2606    2608089 Y   auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY tenant2.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);
 �   ALTER TABLE ONLY tenant2.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq;
       tenant2         postgres    false    236    236         �           2606    2608117     auth_user auth_user_username_key 
   CONSTRAINT     `   ALTER TABLE ONLY tenant2.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);
 K   ALTER TABLE ONLY tenant2.auth_user DROP CONSTRAINT auth_user_username_key;
       tenant2         postgres    false    232         �           2606    2608103 &   django_admin_log django_admin_log_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY tenant2.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);
 Q   ALTER TABLE ONLY tenant2.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
       tenant2         postgres    false    238         �           2606    2607987 E   django_content_type django_content_type_app_label_model_76bd3d3b_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY tenant2.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);
 p   ALTER TABLE ONLY tenant2.django_content_type DROP CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq;
       tenant2         postgres    false    224    224         �           2606    2607985 ,   django_content_type django_content_type_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY tenant2.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);
 W   ALTER TABLE ONLY tenant2.django_content_type DROP CONSTRAINT django_content_type_pkey;
       tenant2         postgres    false    224         �           2606    2607977 (   django_migrations django_migrations_pkey 
   CONSTRAINT     g   ALTER TABLE ONLY tenant2.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);
 S   ALTER TABLE ONLY tenant2.django_migrations DROP CONSTRAINT django_migrations_pkey;
       tenant2         postgres    false    222         �           2606    2608129 "   django_session django_session_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY tenant2.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);
 M   ALTER TABLE ONLY tenant2.django_session DROP CONSTRAINT django_session_pkey;
       tenant2         postgres    false    239                    2606    2608446     fvstuff_items fvstuff_items_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY tenant2.fvstuff_items
    ADD CONSTRAINT fvstuff_items_pkey PRIMARY KEY (id);
 K   ALTER TABLE ONLY tenant2.fvstuff_items DROP CONSTRAINT fvstuff_items_pkey;
       tenant2         postgres    false    259                    2606    2608427 ,   fvstuff_user_groups fvstuff_user_groups_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY tenant2.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_pkey PRIMARY KEY (id);
 W   ALTER TABLE ONLY tenant2.fvstuff_user_groups DROP CONSTRAINT fvstuff_user_groups_pkey;
       tenant2         postgres    false    255                    2606    2608459 F   fvstuff_user_groups fvstuff_user_groups_user_id_group_id_3ae9527c_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY tenant2.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_user_id_group_id_3ae9527c_uniq UNIQUE (user_id, group_id);
 q   ALTER TABLE ONLY tenant2.fvstuff_user_groups DROP CONSTRAINT fvstuff_user_groups_user_id_group_id_3ae9527c_uniq;
       tenant2         postgres    false    255    255                    2606    2608417    fvstuff_user fvstuff_user_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY tenant2.fvstuff_user
    ADD CONSTRAINT fvstuff_user_pkey PRIMARY KEY (id);
 I   ALTER TABLE ONLY tenant2.fvstuff_user DROP CONSTRAINT fvstuff_user_pkey;
       tenant2         postgres    false    253                    2606    2608473 Z   fvstuff_user_user_permissions fvstuff_user_user_permis_user_id_permission_id_783f7839_uniq 
   CONSTRAINT     �   ALTER TABLE ONLY tenant2.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_permis_user_id_permission_id_783f7839_uniq UNIQUE (user_id, permission_id);
 �   ALTER TABLE ONLY tenant2.fvstuff_user_user_permissions DROP CONSTRAINT fvstuff_user_user_permis_user_id_permission_id_783f7839_uniq;
       tenant2         postgres    false    257    257                    2606    2608435 @   fvstuff_user_user_permissions fvstuff_user_user_permissions_pkey 
   CONSTRAINT        ALTER TABLE ONLY tenant2.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_permissions_pkey PRIMARY KEY (id);
 k   ALTER TABLE ONLY tenant2.fvstuff_user_user_permissions DROP CONSTRAINT fvstuff_user_user_permissions_pkey;
       tenant2         postgres    false    257                    2606    2608419 &   fvstuff_user fvstuff_user_username_key 
   CONSTRAINT     f   ALTER TABLE ONLY tenant2.fvstuff_user
    ADD CONSTRAINT fvstuff_user_username_key UNIQUE (username);
 Q   ALTER TABLE ONLY tenant2.fvstuff_user DROP CONSTRAINT fvstuff_user_username_key;
       tenant2         postgres    false    253         �           1259    2607796 *   fvtenants_tenant_schema_name_76f99816_like    INDEX     �   CREATE INDEX fvtenants_tenant_schema_name_76f99816_like ON public.fvtenants_tenant USING btree (schema_name varchar_pattern_ops);
 >   DROP INDEX public.fvtenants_tenant_schema_name_76f99816_like;
       public         postgres    false    201         �           1259    2608162 3   fvtenants_usercredential_tenants_tenant_id_5fc81504    INDEX     �   CREATE INDEX fvtenants_usercredential_tenants_tenant_id_5fc81504 ON public.fvtenants_usercredential_tenants USING btree (tenant_id);
 G   DROP INDEX public.fvtenants_usercredential_tenants_tenant_id_5fc81504;
       public         postgres    false    243         �           1259    2608161 ;   fvtenants_usercredential_tenants_usercredential_id_83064a29    INDEX     �   CREATE INDEX fvtenants_usercredential_tenants_usercredential_id_83064a29 ON public.fvtenants_usercredential_tenants USING btree (usercredential_id);
 O   DROP INDEX public.fvtenants_usercredential_tenants_usercredential_id_83064a29;
       public         postgres    false    243         �           1259    2607879    auth_group_name_a6ea08ec_like    INDEX     i   CREATE INDEX auth_group_name_a6ea08ec_like ON tenant1.auth_group USING btree (name varchar_pattern_ops);
 2   DROP INDEX tenant1.auth_group_name_a6ea08ec_like;
       tenant1         postgres    false    209         �           1259    2607892 (   auth_group_permissions_group_id_b120cbf9    INDEX     p   CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON tenant1.auth_group_permissions USING btree (group_id);
 =   DROP INDEX tenant1.auth_group_permissions_group_id_b120cbf9;
       tenant1         postgres    false    211         �           1259    2607893 -   auth_group_permissions_permission_id_84c5c92e    INDEX     z   CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON tenant1.auth_group_permissions USING btree (permission_id);
 B   DROP INDEX tenant1.auth_group_permissions_permission_id_84c5c92e;
       tenant1         postgres    false    211         �           1259    2607878 (   auth_permission_content_type_id_2f476e4b    INDEX     p   CREATE INDEX auth_permission_content_type_id_2f476e4b ON tenant1.auth_permission USING btree (content_type_id);
 =   DROP INDEX tenant1.auth_permission_content_type_id_2f476e4b;
       tenant1         postgres    false    207         �           1259    2607908 "   auth_user_groups_group_id_97559544    INDEX     d   CREATE INDEX auth_user_groups_group_id_97559544 ON tenant1.auth_user_groups USING btree (group_id);
 7   DROP INDEX tenant1.auth_user_groups_group_id_97559544;
       tenant1         postgres    false    215         �           1259    2607907 !   auth_user_groups_user_id_6a12ed8b    INDEX     b   CREATE INDEX auth_user_groups_user_id_6a12ed8b ON tenant1.auth_user_groups USING btree (user_id);
 6   DROP INDEX tenant1.auth_user_groups_user_id_6a12ed8b;
       tenant1         postgres    false    215         �           1259    2607922 1   auth_user_user_permissions_permission_id_1fbb5f2c    INDEX     �   CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON tenant1.auth_user_user_permissions USING btree (permission_id);
 F   DROP INDEX tenant1.auth_user_user_permissions_permission_id_1fbb5f2c;
       tenant1         postgres    false    217         �           1259    2607921 +   auth_user_user_permissions_user_id_a95ead1b    INDEX     v   CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON tenant1.auth_user_user_permissions USING btree (user_id);
 @   DROP INDEX tenant1.auth_user_user_permissions_user_id_a95ead1b;
       tenant1         postgres    false    217         �           1259    2607949     auth_user_username_6821ab7c_like    INDEX     o   CREATE INDEX auth_user_username_6821ab7c_like ON tenant1.auth_user USING btree (username varchar_pattern_ops);
 5   DROP INDEX tenant1.auth_user_username_6821ab7c_like;
       tenant1         postgres    false    213         �           1259    2607945 )   django_admin_log_content_type_id_c4bce8eb    INDEX     r   CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON tenant1.django_admin_log USING btree (content_type_id);
 >   DROP INDEX tenant1.django_admin_log_content_type_id_c4bce8eb;
       tenant1         postgres    false    219         �           1259    2607946 !   django_admin_log_user_id_c564eba6    INDEX     b   CREATE INDEX django_admin_log_user_id_c564eba6 ON tenant1.django_admin_log USING btree (user_id);
 6   DROP INDEX tenant1.django_admin_log_user_id_c564eba6;
       tenant1         postgres    false    219         �           1259    2607962 #   django_session_expire_date_a5c62663    INDEX     f   CREATE INDEX django_session_expire_date_a5c62663 ON tenant1.django_session USING btree (expire_date);
 8   DROP INDEX tenant1.django_session_expire_date_a5c62663;
       tenant1         postgres    false    220         �           1259    2607961 (   django_session_session_key_c0390e0f_like    INDEX        CREATE INDEX django_session_session_key_c0390e0f_like ON tenant1.django_session USING btree (session_key varchar_pattern_ops);
 =   DROP INDEX tenant1.django_session_session_key_c0390e0f_like;
       tenant1         postgres    false    220         �           1259    2608392 %   fvstuff_user_groups_group_id_2da72214    INDEX     j   CREATE INDEX fvstuff_user_groups_group_id_2da72214 ON tenant1.fvstuff_user_groups USING btree (group_id);
 :   DROP INDEX tenant1.fvstuff_user_groups_group_id_2da72214;
       tenant1         postgres    false    247         �           1259    2608391 $   fvstuff_user_groups_user_id_886cf2a1    INDEX     h   CREATE INDEX fvstuff_user_groups_user_id_886cf2a1 ON tenant1.fvstuff_user_groups USING btree (user_id);
 9   DROP INDEX tenant1.fvstuff_user_groups_user_id_886cf2a1;
       tenant1         postgres    false    247                    1259    2608406 4   fvstuff_user_user_permissions_permission_id_238dce5b    INDEX     �   CREATE INDEX fvstuff_user_user_permissions_permission_id_238dce5b ON tenant1.fvstuff_user_user_permissions USING btree (permission_id);
 I   DROP INDEX tenant1.fvstuff_user_user_permissions_permission_id_238dce5b;
       tenant1         postgres    false    249                    1259    2608405 .   fvstuff_user_user_permissions_user_id_58f7ffd0    INDEX     |   CREATE INDEX fvstuff_user_user_permissions_user_id_58f7ffd0 ON tenant1.fvstuff_user_user_permissions USING btree (user_id);
 C   DROP INDEX tenant1.fvstuff_user_user_permissions_user_id_58f7ffd0;
       tenant1         postgres    false    249         �           1259    2608378 #   fvstuff_user_username_ccda3a42_like    INDEX     u   CREATE INDEX fvstuff_user_username_ccda3a42_like ON tenant1.fvstuff_user USING btree (username varchar_pattern_ops);
 8   DROP INDEX tenant1.fvstuff_user_username_ccda3a42_like;
       tenant1         postgres    false    245         �           1259    2608048    auth_group_name_a6ea08ec_like    INDEX     i   CREATE INDEX auth_group_name_a6ea08ec_like ON tenant2.auth_group USING btree (name varchar_pattern_ops);
 2   DROP INDEX tenant2.auth_group_name_a6ea08ec_like;
       tenant2         postgres    false    228         �           1259    2608061 (   auth_group_permissions_group_id_b120cbf9    INDEX     p   CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON tenant2.auth_group_permissions USING btree (group_id);
 =   DROP INDEX tenant2.auth_group_permissions_group_id_b120cbf9;
       tenant2         postgres    false    230         �           1259    2608062 -   auth_group_permissions_permission_id_84c5c92e    INDEX     z   CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON tenant2.auth_group_permissions USING btree (permission_id);
 B   DROP INDEX tenant2.auth_group_permissions_permission_id_84c5c92e;
       tenant2         postgres    false    230         �           1259    2608047 (   auth_permission_content_type_id_2f476e4b    INDEX     p   CREATE INDEX auth_permission_content_type_id_2f476e4b ON tenant2.auth_permission USING btree (content_type_id);
 =   DROP INDEX tenant2.auth_permission_content_type_id_2f476e4b;
       tenant2         postgres    false    226         �           1259    2608077 "   auth_user_groups_group_id_97559544    INDEX     d   CREATE INDEX auth_user_groups_group_id_97559544 ON tenant2.auth_user_groups USING btree (group_id);
 7   DROP INDEX tenant2.auth_user_groups_group_id_97559544;
       tenant2         postgres    false    234         �           1259    2608076 !   auth_user_groups_user_id_6a12ed8b    INDEX     b   CREATE INDEX auth_user_groups_user_id_6a12ed8b ON tenant2.auth_user_groups USING btree (user_id);
 6   DROP INDEX tenant2.auth_user_groups_user_id_6a12ed8b;
       tenant2         postgres    false    234         �           1259    2608091 1   auth_user_user_permissions_permission_id_1fbb5f2c    INDEX     �   CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON tenant2.auth_user_user_permissions USING btree (permission_id);
 F   DROP INDEX tenant2.auth_user_user_permissions_permission_id_1fbb5f2c;
       tenant2         postgres    false    236         �           1259    2608090 +   auth_user_user_permissions_user_id_a95ead1b    INDEX     v   CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON tenant2.auth_user_user_permissions USING btree (user_id);
 @   DROP INDEX tenant2.auth_user_user_permissions_user_id_a95ead1b;
       tenant2         postgres    false    236         �           1259    2608118     auth_user_username_6821ab7c_like    INDEX     o   CREATE INDEX auth_user_username_6821ab7c_like ON tenant2.auth_user USING btree (username varchar_pattern_ops);
 5   DROP INDEX tenant2.auth_user_username_6821ab7c_like;
       tenant2         postgres    false    232         �           1259    2608114 )   django_admin_log_content_type_id_c4bce8eb    INDEX     r   CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON tenant2.django_admin_log USING btree (content_type_id);
 >   DROP INDEX tenant2.django_admin_log_content_type_id_c4bce8eb;
       tenant2         postgres    false    238         �           1259    2608115 !   django_admin_log_user_id_c564eba6    INDEX     b   CREATE INDEX django_admin_log_user_id_c564eba6 ON tenant2.django_admin_log USING btree (user_id);
 6   DROP INDEX tenant2.django_admin_log_user_id_c564eba6;
       tenant2         postgres    false    238         �           1259    2608131 #   django_session_expire_date_a5c62663    INDEX     f   CREATE INDEX django_session_expire_date_a5c62663 ON tenant2.django_session USING btree (expire_date);
 8   DROP INDEX tenant2.django_session_expire_date_a5c62663;
       tenant2         postgres    false    239         �           1259    2608130 (   django_session_session_key_c0390e0f_like    INDEX        CREATE INDEX django_session_session_key_c0390e0f_like ON tenant2.django_session USING btree (session_key varchar_pattern_ops);
 =   DROP INDEX tenant2.django_session_session_key_c0390e0f_like;
       tenant2         postgres    false    239                    1259    2608461 %   fvstuff_user_groups_group_id_2da72214    INDEX     j   CREATE INDEX fvstuff_user_groups_group_id_2da72214 ON tenant2.fvstuff_user_groups USING btree (group_id);
 :   DROP INDEX tenant2.fvstuff_user_groups_group_id_2da72214;
       tenant2         postgres    false    255                    1259    2608460 $   fvstuff_user_groups_user_id_886cf2a1    INDEX     h   CREATE INDEX fvstuff_user_groups_user_id_886cf2a1 ON tenant2.fvstuff_user_groups USING btree (user_id);
 9   DROP INDEX tenant2.fvstuff_user_groups_user_id_886cf2a1;
       tenant2         postgres    false    255                    1259    2608475 4   fvstuff_user_user_permissions_permission_id_238dce5b    INDEX     �   CREATE INDEX fvstuff_user_user_permissions_permission_id_238dce5b ON tenant2.fvstuff_user_user_permissions USING btree (permission_id);
 I   DROP INDEX tenant2.fvstuff_user_user_permissions_permission_id_238dce5b;
       tenant2         postgres    false    257                    1259    2608474 .   fvstuff_user_user_permissions_user_id_58f7ffd0    INDEX     |   CREATE INDEX fvstuff_user_user_permissions_user_id_58f7ffd0 ON tenant2.fvstuff_user_user_permissions USING btree (user_id);
 C   DROP INDEX tenant2.fvstuff_user_user_permissions_user_id_58f7ffd0;
       tenant2         postgres    false    257                    1259    2608447 #   fvstuff_user_username_ccda3a42_like    INDEX     u   CREATE INDEX fvstuff_user_username_ccda3a42_like ON tenant2.fvstuff_user USING btree (username varchar_pattern_ops);
 8   DROP INDEX tenant2.fvstuff_user_username_ccda3a42_like;
       tenant2         postgres    false    253         0           2606    2608154 U   fvtenants_usercredential_tenants fvtenants_usercreden_tenant_id_5fc81504_fk_fvtenants    FK CONSTRAINT     �   ALTER TABLE ONLY public.fvtenants_usercredential_tenants
    ADD CONSTRAINT fvtenants_usercreden_tenant_id_5fc81504_fk_fvtenants FOREIGN KEY (tenant_id) REFERENCES public.fvtenants_tenant(id) DEFERRABLE INITIALLY DEFERRED;
    ALTER TABLE ONLY public.fvtenants_usercredential_tenants DROP CONSTRAINT fvtenants_usercreden_tenant_id_5fc81504_fk_fvtenants;
       public       postgres    false    243    2957    201         /           2606    2608149 ]   fvtenants_usercredential_tenants fvtenants_usercreden_usercredential_id_83064a29_fk_fvtenants    FK CONSTRAINT     �   ALTER TABLE ONLY public.fvtenants_usercredential_tenants
    ADD CONSTRAINT fvtenants_usercreden_usercredential_id_83064a29_fk_fvtenants FOREIGN KEY (usercredential_id) REFERENCES public.fvtenants_usercredential(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY public.fvtenants_usercredential_tenants DROP CONSTRAINT fvtenants_usercreden_usercredential_id_83064a29_fk_fvtenants;
       public       postgres    false    243    241    3056                    2606    2607885 O   auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY tenant1.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES tenant1.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 z   ALTER TABLE ONLY tenant1.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
       tenant1       postgres    false    2971    211    207                    2606    2607880 P   auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY tenant1.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES tenant1.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 {   ALTER TABLE ONLY tenant1.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
       tenant1       postgres    false    2976    211    209                    2606    2607871 E   auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY tenant1.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES tenant1.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 p   ALTER TABLE ONLY tenant1.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
       tenant1       postgres    false    205    2966    207         !           2606    2607900 D   auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY tenant1.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES tenant1.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 o   ALTER TABLE ONLY tenant1.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
       tenant1       postgres    false    215    2976    209                     2606    2607895 B   auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY tenant1.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES tenant1.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 m   ALTER TABLE ONLY tenant1.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
       tenant1       postgres    false    215    2984    213         #           2606    2607914 S   auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY tenant1.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES tenant1.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 ~   ALTER TABLE ONLY tenant1.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
       tenant1       postgres    false    207    2971    217         "           2606    2607909 V   auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY tenant1.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES tenant1.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY tenant1.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
       tenant1       postgres    false    217    213    2984         $           2606    2607935 G   django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY tenant1.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES tenant1.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 r   ALTER TABLE ONLY tenant1.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
       tenant1       postgres    false    205    2966    219         %           2606    2607940 B   django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY tenant1.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES tenant1.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 m   ALTER TABLE ONLY tenant1.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
       tenant1       postgres    false    213    2984    219         2           2606    2608384 J   fvstuff_user_groups fvstuff_user_groups_group_id_2da72214_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY tenant1.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_group_id_2da72214_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES tenant1.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 u   ALTER TABLE ONLY tenant1.fvstuff_user_groups DROP CONSTRAINT fvstuff_user_groups_group_id_2da72214_fk_auth_group_id;
       tenant1       postgres    false    209    247    2976         1           2606    2608379 K   fvstuff_user_groups fvstuff_user_groups_user_id_886cf2a1_fk_fvstuff_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY tenant1.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_user_id_886cf2a1_fk_fvstuff_user_id FOREIGN KEY (user_id) REFERENCES tenant1.fvstuff_user(id) DEFERRABLE INITIALLY DEFERRED;
 v   ALTER TABLE ONLY tenant1.fvstuff_user_groups DROP CONSTRAINT fvstuff_user_groups_user_id_886cf2a1_fk_fvstuff_user_id;
       tenant1       postgres    false    247    245    3064         4           2606    2608398 V   fvstuff_user_user_permissions fvstuff_user_user_pe_permission_id_238dce5b_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY tenant1.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_pe_permission_id_238dce5b_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES tenant1.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY tenant1.fvstuff_user_user_permissions DROP CONSTRAINT fvstuff_user_user_pe_permission_id_238dce5b_fk_auth_perm;
       tenant1       postgres    false    2971    249    207         3           2606    2608393 P   fvstuff_user_user_permissions fvstuff_user_user_pe_user_id_58f7ffd0_fk_fvstuff_u    FK CONSTRAINT     �   ALTER TABLE ONLY tenant1.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_pe_user_id_58f7ffd0_fk_fvstuff_u FOREIGN KEY (user_id) REFERENCES tenant1.fvstuff_user(id) DEFERRABLE INITIALLY DEFERRED;
 {   ALTER TABLE ONLY tenant1.fvstuff_user_user_permissions DROP CONSTRAINT fvstuff_user_user_pe_user_id_58f7ffd0_fk_fvstuff_u;
       tenant1       postgres    false    3064    245    249         (           2606    2608054 O   auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY tenant2.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES tenant2.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 z   ALTER TABLE ONLY tenant2.auth_group_permissions DROP CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm;
       tenant2       postgres    false    230    226    3018         '           2606    2608049 P   auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY tenant2.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES tenant2.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 {   ALTER TABLE ONLY tenant2.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id;
       tenant2       postgres    false    230    228    3023         &           2606    2608040 E   auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY tenant2.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES tenant2.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 p   ALTER TABLE ONLY tenant2.auth_permission DROP CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co;
       tenant2       postgres    false    3013    224    226         *           2606    2608069 D   auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY tenant2.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES tenant2.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 o   ALTER TABLE ONLY tenant2.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id;
       tenant2       postgres    false    3023    228    234         )           2606    2608064 B   auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY tenant2.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES tenant2.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 m   ALTER TABLE ONLY tenant2.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id;
       tenant2       postgres    false    232    3031    234         ,           2606    2608083 S   auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY tenant2.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES tenant2.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 ~   ALTER TABLE ONLY tenant2.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm;
       tenant2       postgres    false    226    236    3018         +           2606    2608078 V   auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY tenant2.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES tenant2.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY tenant2.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id;
       tenant2       postgres    false    232    3031    236         -           2606    2608104 G   django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co    FK CONSTRAINT     �   ALTER TABLE ONLY tenant2.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES tenant2.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 r   ALTER TABLE ONLY tenant2.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co;
       tenant2       postgres    false    3013    238    224         .           2606    2608109 B   django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY tenant2.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES tenant2.auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 m   ALTER TABLE ONLY tenant2.django_admin_log DROP CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id;
       tenant2       postgres    false    3031    238    232         6           2606    2608453 J   fvstuff_user_groups fvstuff_user_groups_group_id_2da72214_fk_auth_group_id    FK CONSTRAINT     �   ALTER TABLE ONLY tenant2.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_group_id_2da72214_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES tenant2.auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 u   ALTER TABLE ONLY tenant2.fvstuff_user_groups DROP CONSTRAINT fvstuff_user_groups_group_id_2da72214_fk_auth_group_id;
       tenant2       postgres    false    255    3023    228         5           2606    2608448 K   fvstuff_user_groups fvstuff_user_groups_user_id_886cf2a1_fk_fvstuff_user_id    FK CONSTRAINT     �   ALTER TABLE ONLY tenant2.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_user_id_886cf2a1_fk_fvstuff_user_id FOREIGN KEY (user_id) REFERENCES tenant2.fvstuff_user(id) DEFERRABLE INITIALLY DEFERRED;
 v   ALTER TABLE ONLY tenant2.fvstuff_user_groups DROP CONSTRAINT fvstuff_user_groups_user_id_886cf2a1_fk_fvstuff_user_id;
       tenant2       postgres    false    253    3083    255         8           2606    2608467 V   fvstuff_user_user_permissions fvstuff_user_user_pe_permission_id_238dce5b_fk_auth_perm    FK CONSTRAINT     �   ALTER TABLE ONLY tenant2.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_pe_permission_id_238dce5b_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES tenant2.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 �   ALTER TABLE ONLY tenant2.fvstuff_user_user_permissions DROP CONSTRAINT fvstuff_user_user_pe_permission_id_238dce5b_fk_auth_perm;
       tenant2       postgres    false    257    226    3018         7           2606    2608462 P   fvstuff_user_user_permissions fvstuff_user_user_pe_user_id_58f7ffd0_fk_fvstuff_u    FK CONSTRAINT     �   ALTER TABLE ONLY tenant2.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_pe_user_id_58f7ffd0_fk_fvstuff_u FOREIGN KEY (user_id) REFERENCES tenant2.fvstuff_user(id) DEFERRABLE INITIALLY DEFERRED;
 {   ALTER TABLE ONLY tenant2.fvstuff_user_user_permissions DROP CONSTRAINT fvstuff_user_user_pe_user_id_58f7ffd0_fk_fvstuff_u;
       tenant2       postgres    false    257    3083    253                                                                    3251.dat                                                                                            0000600 0004000 0002000 00000002244 13424620645 0014254 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	contenttypes	0001_initial	2019-01-31 09:17:24.327641+00
2	auth	0001_initial	2019-01-31 09:17:24.365449+00
3	admin	0001_initial	2019-01-31 09:17:24.375668+00
4	admin	0002_logentry_remove_auto_add	2019-01-31 09:17:24.389669+00
5	admin	0003_logentry_add_action_flag_choices	2019-01-31 09:17:24.403704+00
6	contenttypes	0002_remove_content_type_name	2019-01-31 09:17:24.420833+00
7	auth	0002_alter_permission_name_max_length	2019-01-31 09:17:24.429138+00
8	auth	0003_alter_user_email_max_length	2019-01-31 09:17:24.444295+00
9	auth	0004_alter_user_username_opts	2019-01-31 09:17:24.456969+00
10	auth	0005_alter_user_last_login_null	2019-01-31 09:17:24.466448+00
11	auth	0006_require_contenttypes_0002	2019-01-31 09:17:24.470435+00
12	auth	0007_alter_validators_add_error_messages	2019-01-31 09:17:24.479737+00
13	auth	0008_alter_user_username_max_length	2019-01-31 09:17:24.489546+00
14	auth	0009_alter_user_last_name_max_length	2019-01-31 09:17:24.49902+00
15	fvtenants	0001_initial	2019-01-31 09:17:24.53333+00
16	sessions	0001_initial	2019-01-31 09:17:24.543454+00
17	fvtenants	0002_usercredential	2019-01-31 09:43:47.682244+00
18	fvstuff	0001_initial	2019-01-31 14:29:38.435371+00
\.


                                                                                                                                                                                                                                                                                                                                                            3253.dat                                                                                            0000600 0004000 0002000 00000000051 13424620645 0014250 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	tenant1	tenant1
2	tenant2	tenant2
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       3293.dat                                                                                            0000600 0004000 0002000 00000000457 13424620645 0014266 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	user@tenant1.com	pbkdf2_sha256$100000$XayE3yXHGucq$2LIAG5KZjbWL4JRXLsysLQr5pVBeGSl+8kmUmXocQ3c=
2	user@tenant2.com	pbkdf2_sha256$100000$XayE3yXHGucq$2LIAG5KZjbWL4JRXLsysLQr5pVBeGSl+8kmUmXocQ3c=
3	multiuser@tenant.com	pbkdf2_sha256$100000$XayE3yXHGucq$2LIAG5KZjbWL4JRXLsysLQr5pVBeGSl+8kmUmXocQ3c=
\.


                                                                                                                                                                                                                 3295.dat                                                                                            0000600 0004000 0002000 00000000035 13424620645 0014260 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	1	1
2	2	2
3	3	1
4	3	2
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   3261.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014246 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3263.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014250 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3259.dat                                                                                            0000600 0004000 0002000 00000002740 13424620645 0014265 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Can add tenant	1	add_tenant
2	Can change tenant	1	change_tenant
3	Can delete tenant	1	delete_tenant
4	Can view tenant	1	view_tenant
5	Can add log entry	2	add_logentry
6	Can change log entry	2	change_logentry
7	Can delete log entry	2	delete_logentry
8	Can view log entry	2	view_logentry
9	Can add permission	3	add_permission
10	Can change permission	3	change_permission
11	Can delete permission	3	delete_permission
12	Can view permission	3	view_permission
13	Can add group	4	add_group
14	Can change group	4	change_group
15	Can delete group	4	delete_group
16	Can view group	4	view_group
17	Can add user	5	add_user
18	Can change user	5	change_user
19	Can delete user	5	delete_user
20	Can view user	5	view_user
21	Can add content type	6	add_contenttype
22	Can change content type	6	change_contenttype
23	Can delete content type	6	delete_contenttype
24	Can view content type	6	view_contenttype
25	Can add session	7	add_session
26	Can change session	7	change_session
27	Can delete session	7	delete_session
28	Can view session	7	view_session
29	Can add user credential	8	add_usercredential
30	Can change user credential	8	change_usercredential
31	Can delete user credential	8	delete_usercredential
32	Can view user credential	8	view_usercredential
33	Can add user	9	add_user
34	Can change user	9	change_user
35	Can delete user	9	delete_user
36	Can view user	9	view_user
37	Can add items	10	add_items
38	Can change items	10	change_items
39	Can delete items	10	delete_items
40	Can view items	10	view_items
\.


                                3265.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014252 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3267.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014254 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3269.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014256 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3271.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014247 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3257.dat                                                                                            0000600 0004000 0002000 00000000275 13424620645 0014264 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	fvtenants	tenant
2	admin	logentry
3	auth	permission
4	auth	group
5	auth	user
6	contenttypes	contenttype
7	sessions	session
8	fvtenants	usercredential
9	fvstuff	user
10	fvstuff	items
\.


                                                                                                                                                                                                                                                                                                                                   3255.dat                                                                                            0000600 0004000 0002000 00000002243 13424620645 0014257 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	contenttypes	0001_initial	2019-01-31 09:20:40.407+00
2	auth	0001_initial	2019-01-31 09:20:40.643697+00
3	admin	0001_initial	2019-01-31 09:20:40.711316+00
4	admin	0002_logentry_remove_auto_add	2019-01-31 09:20:40.725068+00
5	admin	0003_logentry_add_action_flag_choices	2019-01-31 09:20:40.738548+00
6	contenttypes	0002_remove_content_type_name	2019-01-31 09:20:40.760501+00
7	auth	0002_alter_permission_name_max_length	2019-01-31 09:20:40.767408+00
8	auth	0003_alter_user_email_max_length	2019-01-31 09:20:40.778223+00
9	auth	0004_alter_user_username_opts	2019-01-31 09:20:40.790282+00
10	auth	0005_alter_user_last_login_null	2019-01-31 09:20:40.802196+00
11	auth	0006_require_contenttypes_0002	2019-01-31 09:20:40.805381+00
12	auth	0007_alter_validators_add_error_messages	2019-01-31 09:20:40.815342+00
13	auth	0008_alter_user_username_max_length	2019-01-31 09:20:40.835289+00
14	auth	0009_alter_user_last_name_max_length	2019-01-31 09:20:40.846326+00
15	fvtenants	0001_initial	2019-01-31 09:20:40.850784+00
16	sessions	0001_initial	2019-01-31 09:20:40.891189+00
17	fvtenants	0002_usercredential	2019-01-31 14:12:24.728916+00
18	fvstuff	0001_initial	2019-01-31 14:31:12.379853+00
\.


                                                                                                                                                                                                                                                                                                                                                             3272.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014250 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3303.dat                                                                                            0000600 0004000 0002000 00000000045 13424620645 0014247 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	tenant1:item1
2	tenant1:item2
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3297.dat                                                                                            0000600 0004000 0002000 00000000134 13424620645 0014262 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Null	\N	f	user@tenant1.com	user	user	f	t	2019-01-31 19:05:56.862+00	user@tenant1.com
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                    3299.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3301.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014241 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3280.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014247 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3282.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014251 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3278.dat                                                                                            0000600 0004000 0002000 00000002740 13424620645 0014266 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	Can add tenant	1	add_tenant
2	Can change tenant	1	change_tenant
3	Can delete tenant	1	delete_tenant
4	Can view tenant	1	view_tenant
5	Can add log entry	2	add_logentry
6	Can change log entry	2	change_logentry
7	Can delete log entry	2	delete_logentry
8	Can view log entry	2	view_logentry
9	Can add permission	3	add_permission
10	Can change permission	3	change_permission
11	Can delete permission	3	delete_permission
12	Can view permission	3	view_permission
13	Can add group	4	add_group
14	Can change group	4	change_group
15	Can delete group	4	delete_group
16	Can view group	4	view_group
17	Can add user	5	add_user
18	Can change user	5	change_user
19	Can delete user	5	delete_user
20	Can view user	5	view_user
21	Can add content type	6	add_contenttype
22	Can change content type	6	change_contenttype
23	Can delete content type	6	delete_contenttype
24	Can view content type	6	view_contenttype
25	Can add session	7	add_session
26	Can change session	7	change_session
27	Can delete session	7	delete_session
28	Can view session	7	view_session
29	Can add user credential	8	add_usercredential
30	Can change user credential	8	change_usercredential
31	Can delete user credential	8	delete_usercredential
32	Can view user credential	8	view_usercredential
33	Can add user	9	add_user
34	Can change user	9	change_user
35	Can delete user	9	delete_user
36	Can view user	9	view_user
37	Can add items	10	add_items
38	Can change items	10	change_items
39	Can delete items	10	delete_items
40	Can view items	10	view_items
\.


                                3284.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014253 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3286.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014255 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3288.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014257 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3290.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014250 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3276.dat                                                                                            0000600 0004000 0002000 00000000275 13424620645 0014265 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	fvtenants	tenant
2	admin	logentry
3	auth	permission
4	auth	group
5	auth	user
6	contenttypes	contenttype
7	sessions	session
8	fvtenants	usercredential
9	fvstuff	user
10	fvstuff	items
\.


                                                                                                                                                                                                                                                                                                                                   3274.dat                                                                                            0000600 0004000 0002000 00000002244 13424620645 0014261 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	contenttypes	0001_initial	2019-01-31 09:25:49.054203+00
2	auth	0001_initial	2019-01-31 09:25:49.287981+00
3	admin	0001_initial	2019-01-31 09:25:49.3583+00
4	admin	0002_logentry_remove_auto_add	2019-01-31 09:25:49.388063+00
5	admin	0003_logentry_add_action_flag_choices	2019-01-31 09:25:49.400676+00
6	contenttypes	0002_remove_content_type_name	2019-01-31 09:25:49.420261+00
7	auth	0002_alter_permission_name_max_length	2019-01-31 09:25:49.430319+00
8	auth	0003_alter_user_email_max_length	2019-01-31 09:25:49.441474+00
9	auth	0004_alter_user_username_opts	2019-01-31 09:25:49.452798+00
10	auth	0005_alter_user_last_login_null	2019-01-31 09:25:49.463991+00
11	auth	0006_require_contenttypes_0002	2019-01-31 09:25:49.466745+00
12	auth	0007_alter_validators_add_error_messages	2019-01-31 09:25:49.474666+00
13	auth	0008_alter_user_username_max_length	2019-01-31 09:25:49.496643+00
14	auth	0009_alter_user_last_name_max_length	2019-01-31 09:25:49.508428+00
15	fvtenants	0001_initial	2019-01-31 09:25:49.512666+00
16	sessions	0001_initial	2019-01-31 09:25:49.554658+00
17	fvtenants	0002_usercredential	2019-01-31 14:12:50.725869+00
18	fvstuff	0001_initial	2019-01-31 14:31:15.095958+00
\.


                                                                                                                                                                                                                                                                                                                                                            3291.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014251 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3311.dat                                                                                            0000600 0004000 0002000 00000000045 13424620645 0014246 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        1	tenant2:item1
2	tenant2:item2
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3305.dat                                                                                            0000600 0004000 0002000 00000000133 13424620645 0014247 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        2	null	\N	f	user@tenant2.com	user	user	f	t	2019-01-31 19:06:31.72+00	user@tenant2.com
\.


                                                                                                                                                                                                                                                                                                                                                                                                                                     3307.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014247 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           3309.dat                                                                                            0000600 0004000 0002000 00000000005 13424620645 0014251 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        \.


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           restore.sql                                                                                         0000600 0004000 0002000 00000234623 13424620645 0015404 0                                                                                                    ustar 00postgres                        postgres                        0000000 0000000                                                                                                                                                                        --
-- NOTE:
--
-- File paths need to be edited. Search for $$PATH$$ and
-- replace it with the path to the directory containing
-- the extracted data files.
--
--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4 (Debian 10.4-2.pgdg90+1)
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE app_tenant;
--
-- Name: app_tenant; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE app_tenant WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';


ALTER DATABASE app_tenant OWNER TO postgres;

\connect app_tenant

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: tenant1; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA tenant1;


ALTER SCHEMA tenant1 OWNER TO postgres;

--
-- Name: tenant2; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA tenant2;


ALTER SCHEMA tenant2 OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: fvtenants_tenant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fvtenants_tenant (
    id integer NOT NULL,
    schema_name character varying(63) NOT NULL,
    name character varying(100) NOT NULL
);


ALTER TABLE public.fvtenants_tenant OWNER TO postgres;

--
-- Name: fvtenants_tenant_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fvtenants_tenant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fvtenants_tenant_id_seq OWNER TO postgres;

--
-- Name: fvtenants_tenant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fvtenants_tenant_id_seq OWNED BY public.fvtenants_tenant.id;


--
-- Name: fvtenants_usercredential; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fvtenants_usercredential (
    id integer NOT NULL,
    email character varying(254) NOT NULL,
    password character varying(128) NOT NULL
);


ALTER TABLE public.fvtenants_usercredential OWNER TO postgres;

--
-- Name: fvtenants_usercredential_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fvtenants_usercredential_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fvtenants_usercredential_id_seq OWNER TO postgres;

--
-- Name: fvtenants_usercredential_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fvtenants_usercredential_id_seq OWNED BY public.fvtenants_usercredential.id;


--
-- Name: fvtenants_usercredential_tenants; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fvtenants_usercredential_tenants (
    id integer NOT NULL,
    usercredential_id integer NOT NULL,
    tenant_id integer NOT NULL
);


ALTER TABLE public.fvtenants_usercredential_tenants OWNER TO postgres;

--
-- Name: fvtenants_usercredential_tenants_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fvtenants_usercredential_tenants_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fvtenants_usercredential_tenants_id_seq OWNER TO postgres;

--
-- Name: fvtenants_usercredential_tenants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fvtenants_usercredential_tenants_id_seq OWNED BY public.fvtenants_usercredential_tenants.id;


--
-- Name: auth_group; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE tenant1.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.auth_group_id_seq OWNED BY tenant1.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE tenant1.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.auth_group_permissions_id_seq OWNED BY tenant1.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE tenant1.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.auth_permission_id_seq OWNED BY tenant1.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE tenant1.auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE tenant1.auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.auth_user_groups_id_seq OWNED BY tenant1.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.auth_user_id_seq OWNED BY tenant1.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE tenant1.auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.auth_user_user_permissions_id_seq OWNED BY tenant1.auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE tenant1.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.django_admin_log_id_seq OWNED BY tenant1.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE tenant1.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.django_content_type_id_seq OWNED BY tenant1.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE tenant1.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.django_migrations_id_seq OWNED BY tenant1.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE tenant1.django_session OWNER TO postgres;

--
-- Name: fvstuff_items; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.fvstuff_items (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE tenant1.fvstuff_items OWNER TO postgres;

--
-- Name: fvstuff_items_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.fvstuff_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.fvstuff_items_id_seq OWNER TO postgres;

--
-- Name: fvstuff_items_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.fvstuff_items_id_seq OWNED BY tenant1.fvstuff_items.id;


--
-- Name: fvstuff_user; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.fvstuff_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    email character varying(254) NOT NULL
);


ALTER TABLE tenant1.fvstuff_user OWNER TO postgres;

--
-- Name: fvstuff_user_groups; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.fvstuff_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE tenant1.fvstuff_user_groups OWNER TO postgres;

--
-- Name: fvstuff_user_groups_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.fvstuff_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.fvstuff_user_groups_id_seq OWNER TO postgres;

--
-- Name: fvstuff_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.fvstuff_user_groups_id_seq OWNED BY tenant1.fvstuff_user_groups.id;


--
-- Name: fvstuff_user_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.fvstuff_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.fvstuff_user_id_seq OWNER TO postgres;

--
-- Name: fvstuff_user_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.fvstuff_user_id_seq OWNED BY tenant1.fvstuff_user.id;


--
-- Name: fvstuff_user_user_permissions; Type: TABLE; Schema: tenant1; Owner: postgres
--

CREATE TABLE tenant1.fvstuff_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE tenant1.fvstuff_user_user_permissions OWNER TO postgres;

--
-- Name: fvstuff_user_user_permissions_id_seq; Type: SEQUENCE; Schema: tenant1; Owner: postgres
--

CREATE SEQUENCE tenant1.fvstuff_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant1.fvstuff_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: fvstuff_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant1; Owner: postgres
--

ALTER SEQUENCE tenant1.fvstuff_user_user_permissions_id_seq OWNED BY tenant1.fvstuff_user_user_permissions.id;


--
-- Name: auth_group; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE tenant2.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.auth_group_id_seq OWNED BY tenant2.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE tenant2.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.auth_group_permissions_id_seq OWNED BY tenant2.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE tenant2.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.auth_permission_id_seq OWNED BY tenant2.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE tenant2.auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE tenant2.auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.auth_user_groups_id_seq OWNED BY tenant2.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.auth_user_id_seq OWNED BY tenant2.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE tenant2.auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.auth_user_user_permissions_id_seq OWNED BY tenant2.auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE tenant2.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.django_admin_log_id_seq OWNED BY tenant2.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE tenant2.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.django_content_type_id_seq OWNED BY tenant2.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE tenant2.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.django_migrations_id_seq OWNED BY tenant2.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE tenant2.django_session OWNER TO postgres;

--
-- Name: fvstuff_items; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.fvstuff_items (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE tenant2.fvstuff_items OWNER TO postgres;

--
-- Name: fvstuff_items_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.fvstuff_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.fvstuff_items_id_seq OWNER TO postgres;

--
-- Name: fvstuff_items_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.fvstuff_items_id_seq OWNED BY tenant2.fvstuff_items.id;


--
-- Name: fvstuff_user; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.fvstuff_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    email character varying(254) NOT NULL
);


ALTER TABLE tenant2.fvstuff_user OWNER TO postgres;

--
-- Name: fvstuff_user_groups; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.fvstuff_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE tenant2.fvstuff_user_groups OWNER TO postgres;

--
-- Name: fvstuff_user_groups_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.fvstuff_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.fvstuff_user_groups_id_seq OWNER TO postgres;

--
-- Name: fvstuff_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.fvstuff_user_groups_id_seq OWNED BY tenant2.fvstuff_user_groups.id;


--
-- Name: fvstuff_user_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.fvstuff_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.fvstuff_user_id_seq OWNER TO postgres;

--
-- Name: fvstuff_user_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.fvstuff_user_id_seq OWNED BY tenant2.fvstuff_user.id;


--
-- Name: fvstuff_user_user_permissions; Type: TABLE; Schema: tenant2; Owner: postgres
--

CREATE TABLE tenant2.fvstuff_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE tenant2.fvstuff_user_user_permissions OWNER TO postgres;

--
-- Name: fvstuff_user_user_permissions_id_seq; Type: SEQUENCE; Schema: tenant2; Owner: postgres
--

CREATE SEQUENCE tenant2.fvstuff_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tenant2.fvstuff_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: fvstuff_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: tenant2; Owner: postgres
--

ALTER SEQUENCE tenant2.fvstuff_user_user_permissions_id_seq OWNED BY tenant2.fvstuff_user_user_permissions.id;


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: fvtenants_tenant id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fvtenants_tenant ALTER COLUMN id SET DEFAULT nextval('public.fvtenants_tenant_id_seq'::regclass);


--
-- Name: fvtenants_usercredential id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fvtenants_usercredential ALTER COLUMN id SET DEFAULT nextval('public.fvtenants_usercredential_id_seq'::regclass);


--
-- Name: fvtenants_usercredential_tenants id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fvtenants_usercredential_tenants ALTER COLUMN id SET DEFAULT nextval('public.fvtenants_usercredential_tenants_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_group ALTER COLUMN id SET DEFAULT nextval('tenant1.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('tenant1.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_permission ALTER COLUMN id SET DEFAULT nextval('tenant1.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_user ALTER COLUMN id SET DEFAULT nextval('tenant1.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('tenant1.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('tenant1.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.django_admin_log ALTER COLUMN id SET DEFAULT nextval('tenant1.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.django_content_type ALTER COLUMN id SET DEFAULT nextval('tenant1.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.django_migrations ALTER COLUMN id SET DEFAULT nextval('tenant1.django_migrations_id_seq'::regclass);


--
-- Name: fvstuff_items id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_items ALTER COLUMN id SET DEFAULT nextval('tenant1.fvstuff_items_id_seq'::regclass);


--
-- Name: fvstuff_user id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_user ALTER COLUMN id SET DEFAULT nextval('tenant1.fvstuff_user_id_seq'::regclass);


--
-- Name: fvstuff_user_groups id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_user_groups ALTER COLUMN id SET DEFAULT nextval('tenant1.fvstuff_user_groups_id_seq'::regclass);


--
-- Name: fvstuff_user_user_permissions id; Type: DEFAULT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('tenant1.fvstuff_user_user_permissions_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_group ALTER COLUMN id SET DEFAULT nextval('tenant2.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('tenant2.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_permission ALTER COLUMN id SET DEFAULT nextval('tenant2.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_user ALTER COLUMN id SET DEFAULT nextval('tenant2.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('tenant2.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('tenant2.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.django_admin_log ALTER COLUMN id SET DEFAULT nextval('tenant2.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.django_content_type ALTER COLUMN id SET DEFAULT nextval('tenant2.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.django_migrations ALTER COLUMN id SET DEFAULT nextval('tenant2.django_migrations_id_seq'::regclass);


--
-- Name: fvstuff_items id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_items ALTER COLUMN id SET DEFAULT nextval('tenant2.fvstuff_items_id_seq'::regclass);


--
-- Name: fvstuff_user id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_user ALTER COLUMN id SET DEFAULT nextval('tenant2.fvstuff_user_id_seq'::regclass);


--
-- Name: fvstuff_user_groups id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_user_groups ALTER COLUMN id SET DEFAULT nextval('tenant2.fvstuff_user_groups_id_seq'::regclass);


--
-- Name: fvstuff_user_user_permissions id; Type: DEFAULT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('tenant2.fvstuff_user_user_permissions_id_seq'::regclass);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
\.
COPY public.django_migrations (id, app, name, applied) FROM '$$PATH$$/3251.dat';

--
-- Data for Name: fvtenants_tenant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fvtenants_tenant (id, schema_name, name) FROM stdin;
\.
COPY public.fvtenants_tenant (id, schema_name, name) FROM '$$PATH$$/3253.dat';

--
-- Data for Name: fvtenants_usercredential; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fvtenants_usercredential (id, email, password) FROM stdin;
\.
COPY public.fvtenants_usercredential (id, email, password) FROM '$$PATH$$/3293.dat';

--
-- Data for Name: fvtenants_usercredential_tenants; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fvtenants_usercredential_tenants (id, usercredential_id, tenant_id) FROM stdin;
\.
COPY public.fvtenants_usercredential_tenants (id, usercredential_id, tenant_id) FROM '$$PATH$$/3295.dat';

--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.auth_group (id, name) FROM stdin;
\.
COPY tenant1.auth_group (id, name) FROM '$$PATH$$/3261.dat';

--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.
COPY tenant1.auth_group_permissions (id, group_id, permission_id) FROM '$$PATH$$/3263.dat';

--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.auth_permission (id, name, content_type_id, codename) FROM stdin;
\.
COPY tenant1.auth_permission (id, name, content_type_id, codename) FROM '$$PATH$$/3259.dat';

--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
\.
COPY tenant1.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM '$$PATH$$/3265.dat';

--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.auth_user_groups (id, user_id, group_id) FROM stdin;
\.
COPY tenant1.auth_user_groups (id, user_id, group_id) FROM '$$PATH$$/3267.dat';

--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.
COPY tenant1.auth_user_user_permissions (id, user_id, permission_id) FROM '$$PATH$$/3269.dat';

--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.
COPY tenant1.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM '$$PATH$$/3271.dat';

--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.django_content_type (id, app_label, model) FROM stdin;
\.
COPY tenant1.django_content_type (id, app_label, model) FROM '$$PATH$$/3257.dat';

--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.django_migrations (id, app, name, applied) FROM stdin;
\.
COPY tenant1.django_migrations (id, app, name, applied) FROM '$$PATH$$/3255.dat';

--
-- Data for Name: django_session; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.django_session (session_key, session_data, expire_date) FROM stdin;
\.
COPY tenant1.django_session (session_key, session_data, expire_date) FROM '$$PATH$$/3272.dat';

--
-- Data for Name: fvstuff_items; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.fvstuff_items (id, name) FROM stdin;
\.
COPY tenant1.fvstuff_items (id, name) FROM '$$PATH$$/3303.dat';

--
-- Data for Name: fvstuff_user; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.fvstuff_user (id, password, last_login, is_superuser, username, first_name, last_name, is_staff, is_active, date_joined, email) FROM stdin;
\.
COPY tenant1.fvstuff_user (id, password, last_login, is_superuser, username, first_name, last_name, is_staff, is_active, date_joined, email) FROM '$$PATH$$/3297.dat';

--
-- Data for Name: fvstuff_user_groups; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.fvstuff_user_groups (id, user_id, group_id) FROM stdin;
\.
COPY tenant1.fvstuff_user_groups (id, user_id, group_id) FROM '$$PATH$$/3299.dat';

--
-- Data for Name: fvstuff_user_user_permissions; Type: TABLE DATA; Schema: tenant1; Owner: postgres
--

COPY tenant1.fvstuff_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.
COPY tenant1.fvstuff_user_user_permissions (id, user_id, permission_id) FROM '$$PATH$$/3301.dat';

--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.auth_group (id, name) FROM stdin;
\.
COPY tenant2.auth_group (id, name) FROM '$$PATH$$/3280.dat';

--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.
COPY tenant2.auth_group_permissions (id, group_id, permission_id) FROM '$$PATH$$/3282.dat';

--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.auth_permission (id, name, content_type_id, codename) FROM stdin;
\.
COPY tenant2.auth_permission (id, name, content_type_id, codename) FROM '$$PATH$$/3278.dat';

--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
\.
COPY tenant2.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM '$$PATH$$/3284.dat';

--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.auth_user_groups (id, user_id, group_id) FROM stdin;
\.
COPY tenant2.auth_user_groups (id, user_id, group_id) FROM '$$PATH$$/3286.dat';

--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.
COPY tenant2.auth_user_user_permissions (id, user_id, permission_id) FROM '$$PATH$$/3288.dat';

--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.
COPY tenant2.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM '$$PATH$$/3290.dat';

--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.django_content_type (id, app_label, model) FROM stdin;
\.
COPY tenant2.django_content_type (id, app_label, model) FROM '$$PATH$$/3276.dat';

--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.django_migrations (id, app, name, applied) FROM stdin;
\.
COPY tenant2.django_migrations (id, app, name, applied) FROM '$$PATH$$/3274.dat';

--
-- Data for Name: django_session; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.django_session (session_key, session_data, expire_date) FROM stdin;
\.
COPY tenant2.django_session (session_key, session_data, expire_date) FROM '$$PATH$$/3291.dat';

--
-- Data for Name: fvstuff_items; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.fvstuff_items (id, name) FROM stdin;
\.
COPY tenant2.fvstuff_items (id, name) FROM '$$PATH$$/3311.dat';

--
-- Data for Name: fvstuff_user; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.fvstuff_user (id, password, last_login, is_superuser, username, first_name, last_name, is_staff, is_active, date_joined, email) FROM stdin;
\.
COPY tenant2.fvstuff_user (id, password, last_login, is_superuser, username, first_name, last_name, is_staff, is_active, date_joined, email) FROM '$$PATH$$/3305.dat';

--
-- Data for Name: fvstuff_user_groups; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.fvstuff_user_groups (id, user_id, group_id) FROM stdin;
\.
COPY tenant2.fvstuff_user_groups (id, user_id, group_id) FROM '$$PATH$$/3307.dat';

--
-- Data for Name: fvstuff_user_user_permissions; Type: TABLE DATA; Schema: tenant2; Owner: postgres
--

COPY tenant2.fvstuff_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.
COPY tenant2.fvstuff_user_user_permissions (id, user_id, permission_id) FROM '$$PATH$$/3309.dat';

--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 18, true);


--
-- Name: fvtenants_tenant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fvtenants_tenant_id_seq', 2, true);


--
-- Name: fvtenants_usercredential_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fvtenants_usercredential_id_seq', 3, true);


--
-- Name: fvtenants_usercredential_tenants_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fvtenants_usercredential_tenants_id_seq', 4, true);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.auth_permission_id_seq', 40, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.auth_user_id_seq', 6, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.django_content_type_id_seq', 10, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.django_migrations_id_seq', 18, true);


--
-- Name: fvstuff_items_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.fvstuff_items_id_seq', 2, true);


--
-- Name: fvstuff_user_groups_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.fvstuff_user_groups_id_seq', 1, false);


--
-- Name: fvstuff_user_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.fvstuff_user_id_seq', 1, true);


--
-- Name: fvstuff_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: tenant1; Owner: postgres
--

SELECT pg_catalog.setval('tenant1.fvstuff_user_user_permissions_id_seq', 1, false);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.auth_permission_id_seq', 40, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.auth_user_id_seq', 2, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.django_content_type_id_seq', 10, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.django_migrations_id_seq', 18, true);


--
-- Name: fvstuff_items_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.fvstuff_items_id_seq', 2, true);


--
-- Name: fvstuff_user_groups_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.fvstuff_user_groups_id_seq', 1, false);


--
-- Name: fvstuff_user_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.fvstuff_user_id_seq', 2, true);


--
-- Name: fvstuff_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: tenant2; Owner: postgres
--

SELECT pg_catalog.setval('tenant2.fvstuff_user_user_permissions_id_seq', 1, false);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: fvtenants_tenant fvtenants_tenant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fvtenants_tenant
    ADD CONSTRAINT fvtenants_tenant_pkey PRIMARY KEY (id);


--
-- Name: fvtenants_tenant fvtenants_tenant_schema_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fvtenants_tenant
    ADD CONSTRAINT fvtenants_tenant_schema_name_key UNIQUE (schema_name);


--
-- Name: fvtenants_usercredential fvtenants_usercredential_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fvtenants_usercredential
    ADD CONSTRAINT fvtenants_usercredential_pkey PRIMARY KEY (id);


--
-- Name: fvtenants_usercredential_tenants fvtenants_usercredential_tenants_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fvtenants_usercredential_tenants
    ADD CONSTRAINT fvtenants_usercredential_tenants_pkey PRIMARY KEY (id);


--
-- Name: fvtenants_usercredential_tenants fvtenants_usercredential_usercredential_id_tenant_daf12375_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fvtenants_usercredential_tenants
    ADD CONSTRAINT fvtenants_usercredential_usercredential_id_tenant_daf12375_uniq UNIQUE (usercredential_id, tenant_id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: fvstuff_items fvstuff_items_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_items
    ADD CONSTRAINT fvstuff_items_pkey PRIMARY KEY (id);


--
-- Name: fvstuff_user_groups fvstuff_user_groups_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_pkey PRIMARY KEY (id);


--
-- Name: fvstuff_user_groups fvstuff_user_groups_user_id_group_id_3ae9527c_uniq; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_user_id_group_id_3ae9527c_uniq UNIQUE (user_id, group_id);


--
-- Name: fvstuff_user fvstuff_user_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_user
    ADD CONSTRAINT fvstuff_user_pkey PRIMARY KEY (id);


--
-- Name: fvstuff_user_user_permissions fvstuff_user_user_permis_user_id_permission_id_783f7839_uniq; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_permis_user_id_permission_id_783f7839_uniq UNIQUE (user_id, permission_id);


--
-- Name: fvstuff_user_user_permissions fvstuff_user_user_permissions_pkey; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: fvstuff_user fvstuff_user_username_key; Type: CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_user
    ADD CONSTRAINT fvstuff_user_username_key UNIQUE (username);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: fvstuff_items fvstuff_items_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_items
    ADD CONSTRAINT fvstuff_items_pkey PRIMARY KEY (id);


--
-- Name: fvstuff_user_groups fvstuff_user_groups_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_pkey PRIMARY KEY (id);


--
-- Name: fvstuff_user_groups fvstuff_user_groups_user_id_group_id_3ae9527c_uniq; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_user_id_group_id_3ae9527c_uniq UNIQUE (user_id, group_id);


--
-- Name: fvstuff_user fvstuff_user_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_user
    ADD CONSTRAINT fvstuff_user_pkey PRIMARY KEY (id);


--
-- Name: fvstuff_user_user_permissions fvstuff_user_user_permis_user_id_permission_id_783f7839_uniq; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_permis_user_id_permission_id_783f7839_uniq UNIQUE (user_id, permission_id);


--
-- Name: fvstuff_user_user_permissions fvstuff_user_user_permissions_pkey; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: fvstuff_user fvstuff_user_username_key; Type: CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_user
    ADD CONSTRAINT fvstuff_user_username_key UNIQUE (username);


--
-- Name: fvtenants_tenant_schema_name_76f99816_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fvtenants_tenant_schema_name_76f99816_like ON public.fvtenants_tenant USING btree (schema_name varchar_pattern_ops);


--
-- Name: fvtenants_usercredential_tenants_tenant_id_5fc81504; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fvtenants_usercredential_tenants_tenant_id_5fc81504 ON public.fvtenants_usercredential_tenants USING btree (tenant_id);


--
-- Name: fvtenants_usercredential_tenants_usercredential_id_83064a29; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fvtenants_usercredential_tenants_usercredential_id_83064a29 ON public.fvtenants_usercredential_tenants USING btree (usercredential_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON tenant1.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON tenant1.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON tenant1.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON tenant1.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX auth_user_groups_group_id_97559544 ON tenant1.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON tenant1.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON tenant1.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON tenant1.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX auth_user_username_6821ab7c_like ON tenant1.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON tenant1.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON tenant1.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON tenant1.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON tenant1.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: fvstuff_user_groups_group_id_2da72214; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX fvstuff_user_groups_group_id_2da72214 ON tenant1.fvstuff_user_groups USING btree (group_id);


--
-- Name: fvstuff_user_groups_user_id_886cf2a1; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX fvstuff_user_groups_user_id_886cf2a1 ON tenant1.fvstuff_user_groups USING btree (user_id);


--
-- Name: fvstuff_user_user_permissions_permission_id_238dce5b; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX fvstuff_user_user_permissions_permission_id_238dce5b ON tenant1.fvstuff_user_user_permissions USING btree (permission_id);


--
-- Name: fvstuff_user_user_permissions_user_id_58f7ffd0; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX fvstuff_user_user_permissions_user_id_58f7ffd0 ON tenant1.fvstuff_user_user_permissions USING btree (user_id);


--
-- Name: fvstuff_user_username_ccda3a42_like; Type: INDEX; Schema: tenant1; Owner: postgres
--

CREATE INDEX fvstuff_user_username_ccda3a42_like ON tenant1.fvstuff_user USING btree (username varchar_pattern_ops);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON tenant2.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON tenant2.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON tenant2.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON tenant2.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX auth_user_groups_group_id_97559544 ON tenant2.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON tenant2.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON tenant2.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON tenant2.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX auth_user_username_6821ab7c_like ON tenant2.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON tenant2.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON tenant2.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON tenant2.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON tenant2.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: fvstuff_user_groups_group_id_2da72214; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX fvstuff_user_groups_group_id_2da72214 ON tenant2.fvstuff_user_groups USING btree (group_id);


--
-- Name: fvstuff_user_groups_user_id_886cf2a1; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX fvstuff_user_groups_user_id_886cf2a1 ON tenant2.fvstuff_user_groups USING btree (user_id);


--
-- Name: fvstuff_user_user_permissions_permission_id_238dce5b; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX fvstuff_user_user_permissions_permission_id_238dce5b ON tenant2.fvstuff_user_user_permissions USING btree (permission_id);


--
-- Name: fvstuff_user_user_permissions_user_id_58f7ffd0; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX fvstuff_user_user_permissions_user_id_58f7ffd0 ON tenant2.fvstuff_user_user_permissions USING btree (user_id);


--
-- Name: fvstuff_user_username_ccda3a42_like; Type: INDEX; Schema: tenant2; Owner: postgres
--

CREATE INDEX fvstuff_user_username_ccda3a42_like ON tenant2.fvstuff_user USING btree (username varchar_pattern_ops);


--
-- Name: fvtenants_usercredential_tenants fvtenants_usercreden_tenant_id_5fc81504_fk_fvtenants; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fvtenants_usercredential_tenants
    ADD CONSTRAINT fvtenants_usercreden_tenant_id_5fc81504_fk_fvtenants FOREIGN KEY (tenant_id) REFERENCES public.fvtenants_tenant(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fvtenants_usercredential_tenants fvtenants_usercreden_usercredential_id_83064a29_fk_fvtenants; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fvtenants_usercredential_tenants
    ADD CONSTRAINT fvtenants_usercreden_usercredential_id_83064a29_fk_fvtenants FOREIGN KEY (usercredential_id) REFERENCES public.fvtenants_usercredential(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES tenant1.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES tenant1.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES tenant1.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES tenant1.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES tenant1.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES tenant1.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES tenant1.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES tenant1.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES tenant1.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fvstuff_user_groups fvstuff_user_groups_group_id_2da72214_fk_auth_group_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_group_id_2da72214_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES tenant1.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fvstuff_user_groups fvstuff_user_groups_user_id_886cf2a1_fk_fvstuff_user_id; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_user_id_886cf2a1_fk_fvstuff_user_id FOREIGN KEY (user_id) REFERENCES tenant1.fvstuff_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fvstuff_user_user_permissions fvstuff_user_user_pe_permission_id_238dce5b_fk_auth_perm; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_pe_permission_id_238dce5b_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES tenant1.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fvstuff_user_user_permissions fvstuff_user_user_pe_user_id_58f7ffd0_fk_fvstuff_u; Type: FK CONSTRAINT; Schema: tenant1; Owner: postgres
--

ALTER TABLE ONLY tenant1.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_pe_user_id_58f7ffd0_fk_fvstuff_u FOREIGN KEY (user_id) REFERENCES tenant1.fvstuff_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES tenant2.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES tenant2.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES tenant2.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES tenant2.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES tenant2.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES tenant2.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES tenant2.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES tenant2.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES tenant2.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fvstuff_user_groups fvstuff_user_groups_group_id_2da72214_fk_auth_group_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_group_id_2da72214_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES tenant2.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fvstuff_user_groups fvstuff_user_groups_user_id_886cf2a1_fk_fvstuff_user_id; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_user_groups
    ADD CONSTRAINT fvstuff_user_groups_user_id_886cf2a1_fk_fvstuff_user_id FOREIGN KEY (user_id) REFERENCES tenant2.fvstuff_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fvstuff_user_user_permissions fvstuff_user_user_pe_permission_id_238dce5b_fk_auth_perm; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_pe_permission_id_238dce5b_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES tenant2.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: fvstuff_user_user_permissions fvstuff_user_user_pe_user_id_58f7ffd0_fk_fvstuff_u; Type: FK CONSTRAINT; Schema: tenant2; Owner: postgres
--

ALTER TABLE ONLY tenant2.fvstuff_user_user_permissions
    ADD CONSTRAINT fvstuff_user_user_pe_user_id_58f7ffd0_fk_fvstuff_u FOREIGN KEY (user_id) REFERENCES tenant2.fvstuff_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             