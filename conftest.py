import csv

import pytest

from django.core.management import call_command


# @pytest.fixture(scope='class')
# def django_db_setup(django_db_setup, django_db_blocker):
#     with django_db_blocker.unblock():
#         call_command('loaddata', 'groups.json')
#         call_command('loaddata', 'user.json')
#         call_command('loaddata', 'account.json')
#         call_command('loaddata', 'voucher.json')
#         call_command('loaddata', 'voucher_history.json')
#         call_command('loaddata', 'invoice_data.json')
#         call_command('loaddata', 'invoice_scan.json')
#         call_command('loaddata', 'feedback.json')
