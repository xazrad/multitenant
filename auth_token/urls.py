from django.urls import path

from .views import ObtainJSONWebToken


app_name = 'auth_token'

urlpatterns = [
    path('jwt/', ObtainJSONWebToken.as_view(), name='jwt-auth'),
]
