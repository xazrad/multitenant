from django.apps import AppConfig


class AuthTokenConfig(AppConfig):
    name = 'auth_token'
    verbose_name = 'Token'
