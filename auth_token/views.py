from rest_framework_jwt.views import ObtainJSONWebToken as OriginObtainJSONWebToken


from .serializers import JSONWebTokenSerializer


class ObtainJSONWebToken(OriginObtainJSONWebToken):
    serializer_class = JSONWebTokenSerializer
