from calendar import timegm
from datetime import datetime

import jwt
from django_tenants.utils import schema_context
from rest_framework_jwt.settings import api_settings

from django.conf import settings
from django.contrib.auth import get_user_model

from fvtenants.models import UserCredential


def user_jwt_secret_key(user):
    if isinstance(user, UserCredential):
        password = user.password
    else:
        password = UserCredential.objects.filter(email=user.username).values('password').first()['password']
    key = '{}{}'.format(password, settings.SECRET_KEY)
    return key


def jwt_get_secret_key(payload=None):
    """
    For enhanced security you may want to use a secret key based on user.

    This way you have an option to logout only this user if:
        - token is compromised
        - password is changed
        - etc.
    """
    if api_settings.JWT_GET_USER_SECRET_KEY:
        user = UserCredential.objects.get(pk=payload.get('user_id'))
        key = str(api_settings.JWT_GET_USER_SECRET_KEY(user))
        return key
    return api_settings.JWT_SECRET_KEY


def jwt_encode_handler(payload):
    key = api_settings.JWT_PRIVATE_KEY or jwt_get_secret_key(payload)
    return jwt.encode(
        payload,
        key,
        api_settings.JWT_ALGORITHM
    ).decode('utf-8')


def jwt_payload_handler(user_creds):
    payload = {
        'user_id': None,
        'username': user_creds.email,
        'exp': datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA,
        'tenants': list(user_creds.tenants.values('pk', 'name')),
        'current_tenant': None
    }
    if payload['tenants']:
        payload['current_tenant'] = payload['tenants'][0]['pk']
        schema = payload['tenants'][0]['name']
        with schema_context(schema):
            USER_MODEL = get_user_model()
            payload['user_id'] =USER_MODEL.objects.filter(username=user_creds.email).values('pk').first()['pk']
    # Include original issued at time for a brand new token,
    # to allow token refresh
    if api_settings.JWT_ALLOW_REFRESH:
        payload['orig_iat'] = timegm(
            datetime.utcnow().utctimetuple()
        )

    if api_settings.JWT_AUDIENCE is not None:
        payload['aud'] = api_settings.JWT_AUDIENCE

    if api_settings.JWT_ISSUER is not None:
        payload['iss'] = api_settings.JWT_ISSUER

    return payload
